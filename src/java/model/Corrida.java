package model;

import java.sql.Date;

public class Corrida {

    private int id;
    private Date data;
    private int id_req_org;
    private int id_pf;
    private double distancia;
    private double valor;
    private int nota_do_clt;
    private int nota_do_tax;
    private boolean emAndamento;
    private boolean encerrada;

    public Date getData() {
        return data;
    }

    public void setData(Date data) {
        this.data = data;
    }

    public boolean isEncerrada() {
        return encerrada;
    }

    public void setEncerrada(boolean encerrada) {
        this.encerrada = encerrada;
    }

    public int getId_pf() {
        return id_pf;
    }

    public void setId_pf(int id_pf) {
        this.id_pf = id_pf;
    }

    public boolean isEmAndamento() {
        return emAndamento;
    }

    public void setEmAndamento(boolean emAndamento) {
        this.emAndamento = emAndamento;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public int getId_req_org() {
        return id_req_org;
    }

    public void setId_req_org(int id_req_org) {
        this.id_req_org = id_req_org;
    }

    public double getDistancia() {
        return distancia;
    }

    public void setDistancia(double distancia) {
        this.distancia = distancia;
    }

    public double getValor() {
        return valor;
    }

    public void setValor(double valor) {
        this.valor = valor;
    }

    public int getNota_do_clt() {
        return nota_do_clt;
    }

    public void setNota_do_clt(int nota_do_clt) {
        this.nota_do_clt = nota_do_clt;
    }

    public int getNota_do_tax() {
        return nota_do_tax;
    }

    public void setNota_do_tax(int nota_do_tax) {
        this.nota_do_tax = nota_do_tax;
    }
    
}
