package model;

public class Query3_5 {
    
    private String top_10_media_nota_cliente_nome;
    private double top_10_media_nota_cliente_media;

    public String getTop_10_media_nota_cliente_nome() {
        return top_10_media_nota_cliente_nome;
    }

    public void setTop_10_media_nota_cliente_nome(String top_10_media_nota_cliente_nome) {
        this.top_10_media_nota_cliente_nome = top_10_media_nota_cliente_nome;
    }

    public double getTop_10_media_nota_cliente_media() {
        return top_10_media_nota_cliente_media;
    }

    public void setTop_10_media_nota_cliente_media(double top_10_media_nota_cliente_media) {
        this.top_10_media_nota_cliente_media = top_10_media_nota_cliente_media;
    }
    
}
