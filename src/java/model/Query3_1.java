package model;

public class Query3_1 {
    
    private String top_10_qtdade_corridas_cliente_nome;
    private int top_10_qtdade_corridas_cliente_qtdade;

    public String getTop_10_qtdade_corridas_cliente_nome() {
        return top_10_qtdade_corridas_cliente_nome;
    }

    public void setTop_10_qtdade_corridas_cliente_nome(String top_10_qtdade_corridas_cliente_nome) {
        this.top_10_qtdade_corridas_cliente_nome = top_10_qtdade_corridas_cliente_nome;
    }

    public int getTop_10_qtdade_corridas_cliente_qtdade() {
        return top_10_qtdade_corridas_cliente_qtdade;
    }

    public void setTop_10_qtdade_corridas_cliente_qtdade(int top_10_qtdade_corridas_cliente_qtdade) {
        this.top_10_qtdade_corridas_cliente_qtdade = top_10_qtdade_corridas_cliente_qtdade;
    }
    
}
