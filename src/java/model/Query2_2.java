package model;

public class Query2_2 {
    
    private int qtdade_total_corr_modelo;
    private double valor_cpkr_media_modelo;
    private double valor_cpkr_min_modelo;
    private double valor_cpkr_max_modelo;

    public int getQtdade_total_corr_modelo() {
        return qtdade_total_corr_modelo;
    }

    public void setQtdade_total_corr_modelo(int qtdade_total_corr_modelo) {
        this.qtdade_total_corr_modelo = qtdade_total_corr_modelo;
    }

    public double getValor_cpkr_media_modelo() {
        return valor_cpkr_media_modelo;
    }

    public void setValor_cpkr_media_modelo(double valor_cpkr_media_modelo) {
        this.valor_cpkr_media_modelo = valor_cpkr_media_modelo;
    }

    public double getValor_cpkr_min_modelo() {
        return valor_cpkr_min_modelo;
    }

    public void setValor_cpkr_min_modelo(double valor_cpkr_min_modelo) {
        this.valor_cpkr_min_modelo = valor_cpkr_min_modelo;
    }

    public double getValor_cpkr_max_modelo() {
        return valor_cpkr_max_modelo;
    }

    public void setValor_cpkr_max_modelo(double valor_cpkr_max_modelo) {
        this.valor_cpkr_max_modelo = valor_cpkr_max_modelo;
    }
    
}
