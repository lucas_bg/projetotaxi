package model;

public class Query3_4 {
    
    private String top_10_soma_valores_taxista_nome;
    private double top_10_soma_valores_taxista_soma;

    public String getTop_10_soma_valores_taxista_nome() {
        return top_10_soma_valores_taxista_nome;
    }

    public void setTop_10_soma_valores_taxista_nome(String top_10_soma_valores_taxista_nome) {
        this.top_10_soma_valores_taxista_nome = top_10_soma_valores_taxista_nome;
    }

    public double getTop_10_soma_valores_taxista_soma() {
        return top_10_soma_valores_taxista_soma;
    }

    public void setTop_10_soma_valores_taxista_soma(double top_10_soma_valores_taxista_soma) {
        this.top_10_soma_valores_taxista_soma = top_10_soma_valores_taxista_soma;
    }
    
}
