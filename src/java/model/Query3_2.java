package model;

public class Query3_2 {
    
    private String top_10_qtdade_corridas_taxista_nome;
    private int top_10_qtdade_corridas_taxista_qtdade;

    public String getTop_10_qtdade_corridas_taxista_nome() {
        return top_10_qtdade_corridas_taxista_nome;
    }

    public void setTop_10_qtdade_corridas_taxista_nome(String top_10_qtdade_corridas_taxista_nome) {
        this.top_10_qtdade_corridas_taxista_nome = top_10_qtdade_corridas_taxista_nome;
    }

    public int getTop_10_qtdade_corridas_taxista_qtdade() {
        return top_10_qtdade_corridas_taxista_qtdade;
    }

    public void setTop_10_qtdade_corridas_taxista_qtdade(int top_10_qtdade_corridas_taxista_qtdade) {
        this.top_10_qtdade_corridas_taxista_qtdade = top_10_qtdade_corridas_taxista_qtdade;
    }
    
}
