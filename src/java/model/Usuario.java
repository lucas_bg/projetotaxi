package model;

import java.io.InputStream;
import java.sql.Date;

public class Usuario {
    
    private int id;
    private String login;
    private String senha;
    private String pnome;
    private String snome;
    private Date dtNasc;
    private String tel;
    private String sexo;
    private boolean ativo;
    private String cpf;
    private InputStream foto;
    private long fotoSize;
    private byte[] fotoAsByteArray;

    public byte[] getFotoAsByteArray() {
        return fotoAsByteArray;
    }

    public void setFotoAsByteArray(byte[] fotoAsByteArray) {
        this.fotoAsByteArray = fotoAsByteArray;
    }

    public long getFotoSize() {
        return fotoSize;
    }

    public void setFotoSize(long fotoSize) {
        this.fotoSize = fotoSize;
    }

    public InputStream getFoto() {
        return foto;
    }

    public void setFoto(InputStream foto) {
        this.foto = foto;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getLogin() {
        return login;
    }

    public void setLogin(String login) {
        this.login = login;
    }

    public String getSenha() {
        return senha;
    }

    public void setSenha(String senha) {
        this.senha = senha;
    }

    public String getPnome() {
        return pnome;
    }

    public void setPnome(String pnome) {
        this.pnome = pnome;
    }

    public String getSnome() {
        return snome;
    }

    public void setSnome(String snome) {
        this.snome = snome;
    }

    public Date getDtNasc() {
        return dtNasc;
    }

    public void setDtNasc(Date dtNasc) {
        this.dtNasc = dtNasc;
    }

    public String getTel() {
        return tel;
    }

    public void setTel(String tel) {
        this.tel = tel;
    }

    public String getSexo() {
        return sexo;
    }

    public void setSexo(String sexo) {
        this.sexo = sexo;
    }

    public boolean isAtivo() {
        return ativo;
    }

    public void setAtivo(boolean ativo) {
        this.ativo = ativo;
    }

    public String getCpf() {
        return cpf;
    }

    public void setCpf(String cpf) {
        this.cpf = cpf;
    }
    
}
