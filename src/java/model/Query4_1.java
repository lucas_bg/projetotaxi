package model;

public class Query4_1 {
    
    private int top_10_cliente_fiel_qt_vezes;
    private String top_10_cliente_fiel_login_clt;
    private String top_10_cliente_fiel_login_tax;

    public int getTop_10_cliente_fiel_qt_vezes() {
        return top_10_cliente_fiel_qt_vezes;
    }

    public void setTop_10_cliente_fiel_qt_vezes(int top_10_cliente_fiel_qt_vezes) {
        this.top_10_cliente_fiel_qt_vezes = top_10_cliente_fiel_qt_vezes;
    }

    public String getTop_10_cliente_fiel_login_clt() {
        return top_10_cliente_fiel_login_clt;
    }

    public void setTop_10_cliente_fiel_login_clt(String top_10_cliente_fiel_login_clt) {
        this.top_10_cliente_fiel_login_clt = top_10_cliente_fiel_login_clt;
    }

    public String getTop_10_cliente_fiel_login_tax() {
        return top_10_cliente_fiel_login_tax;
    }

    public void setTop_10_cliente_fiel_login_tax(String top_10_cliente_fiel_login_tax) {
        this.top_10_cliente_fiel_login_tax = top_10_cliente_fiel_login_tax;
    }
    
}
