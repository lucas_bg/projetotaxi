package model;

public class FindAndHead {

    private Ponto find;
    private Ponto head;
    private int i;

    public int getI() {
        return i;
    }

    public void setI(int i) {
        this.i = i;
    }

    public Ponto getFind() {
        return find;
    }

    public void setFind(Ponto find) {
        this.find = find;
    }

    public Ponto getHead() {
        return head;
    }

    public void setHead(Ponto head) {
        this.head = head;
    }
    
}
