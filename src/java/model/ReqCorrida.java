package model;

public class ReqCorrida {
    
    private int id;
    private int id_clt;
    private int id_tax;
    private int id_p0;
    private int id_pi;
    private boolean aceita;
    private boolean analisada;
    private int traj_esc;

    public int getId_p0() {
        return id_p0;
    }

    public void setId_p0(int id_p0) {
        this.id_p0 = id_p0;
    }

    public int getId_pi() {
        return id_pi;
    }

    public void setId_pi(int id_pi) {
        this.id_pi = id_pi;
    }

    public int getTraj_esc() {
        return traj_esc;
    }

    public void setTraj_esc(int traj_esc) {
        this.traj_esc = traj_esc;
    }
    
    public boolean isAnalisada() {
        return analisada;
    }

    public void setAnalisada(boolean analisada) {
        this.analisada = analisada;
    }

    public int getId_clt() {
        return id_clt;
    }

    public void setId_clt(int id_clt) {
        this.id_clt = id_clt;
    }

    public int getId_tax() {
        return id_tax;
    }

    public void setId_tax(int id_tax) {
        this.id_tax = id_tax;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public boolean isAceita() {
        return aceita;
    }

    public void setAceita(boolean aceita) {
        this.aceita = aceita;
    }
    
}
