package model;

import java.sql.Date;

public class Historico {
    
    private double distancia;
    private double valor;
    private Date data;
    //CLIENTE
    private int nota_do_tax;
    private String tax_pname;
    private String tax_sname;
    //TAXISTA
    private int nota_do_clt;
    private String clt_pname;
    private String clt_sname;

    public double getDistancia() {
        return distancia;
    }

    public void setDistancia(double distancia) {
        this.distancia = distancia;
    }

    public double getValor() {
        return valor;
    }

    public void setValor(double valor) {
        this.valor = valor;
    }

    public Date getData() {
        return data;
    }

    public void setData(Date data) {
        this.data = data;
    }

    public int getNota_do_tax() {
        return nota_do_tax;
    }

    public void setNota_do_tax(int nota_do_tax) {
        this.nota_do_tax = nota_do_tax;
    }

    public String getTax_pname() {
        return tax_pname;
    }

    public void setTax_pname(String tax_pname) {
        this.tax_pname = tax_pname;
    }

    public String getTax_sname() {
        return tax_sname;
    }

    public void setTax_sname(String tax_sname) {
        this.tax_sname = tax_sname;
    }

    public int getNota_do_clt() {
        return nota_do_clt;
    }

    public void setNota_do_clt(int nota_do_clt) {
        this.nota_do_clt = nota_do_clt;
    }

    public String getClt_pname() {
        return clt_pname;
    }

    public void setClt_pname(String clt_pname) {
        this.clt_pname = clt_pname;
    }

    public String getClt_sname() {
        return clt_sname;
    }

    public void setClt_sname(String clt_sname) {
        this.clt_sname = clt_sname;
    }
    
}
