package model;

import java.io.InputStream;

public class Veiculo {
    
    private int id;
    private String renavam;
    private String ano;
    private String cor;
    private String placa;
    private String marca;
    private String modelo;
    private int cap;
    private int id_usuario;
    private InputStream foto;
    private long fotoSize;
    private byte[] fotoAsByteArray;

    public byte[] getFotoAsByteArray() {
        return fotoAsByteArray;
    }

    public void setFotoAsByteArray(byte[] fotoAsByteArray) {
        this.fotoAsByteArray = fotoAsByteArray;
    }

    public long getFotoSize() {
        return fotoSize;
    }

    public void setFotoSize(long fotoSize) {
        this.fotoSize = fotoSize;
    }

    public InputStream getFoto() {
        return foto;
    }

    public void setFoto(InputStream foto) {
        this.foto = foto;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getRenavam() {
        return renavam;
    }

    public void setRenavam(String renavam) {
        this.renavam = renavam;
    }

    public String getAno() {
        return ano;
    }

    public void setAno(String ano) {
        this.ano = ano;
    }

    public String getCor() {
        return cor;
    }

    public void setCor(String cor) {
        this.cor = cor;
    }

    public String getPlaca() {
        return placa;
    }

    public void setPlaca(String placa) {
        this.placa = placa;
    }

    public String getMarca() {
        return marca;
    }

    public void setMarca(String marca) {
        this.marca = marca;
    }

    public String getModelo() {
        return modelo;
    }

    public void setModelo(String modelo) {
        this.modelo = modelo;
    }

    public int getCap() {
        return cap;
    }

    public void setCap(int cap) {
        this.cap = cap;
    }

    public int getId_usuario() {
        return id_usuario;
    }

    public void setId_usuario(int id_usuario) {
        this.id_usuario = id_usuario;
    }
    
}
