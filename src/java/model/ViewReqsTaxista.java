package model;

import java.sql.Date;

public class ViewReqsTaxista {

    private String pnome_clt;
    private String snome_clt;
    private String tel_clt;
    private String sexo_clt;
    private Date dtNasc_clt;
    private int id_clt;
    private int id_req;
    private int id_tax;
    private double lat_req;
    private double lng_req;
    private double lat_dest;
    private double lng_dest;
    private int id_corr;

    public double getLat_dest() {
        return lat_dest;
    }

    public void setLat_dest(double lat_dest) {
        this.lat_dest = lat_dest;
    }

    public double getLng_dest() {
        return lng_dest;
    }

    public void setLng_dest(double lng_dest) {
        this.lng_dest = lng_dest;
    }

    public int getId_corr() {
        return id_corr;
    }

    public void setId_corr(int id_corr) {
        this.id_corr = id_corr;
    }

    public String getPnome_clt() {
        return pnome_clt;
    }

    public void setPnome_clt(String pnome_clt) {
        this.pnome_clt = pnome_clt;
    }

    public String getSnome_clt() {
        return snome_clt;
    }

    public void setSnome_clt(String snome_clt) {
        this.snome_clt = snome_clt;
    }

    public String getTel_clt() {
        return tel_clt;
    }

    public void setTel_clt(String tel_clt) {
        this.tel_clt = tel_clt;
    }

    public String getSexo_clt() {
        return sexo_clt;
    }

    public void setSexo_clt(String sexo_clt) {
        this.sexo_clt = sexo_clt;
    }

    public Date getDtNasc_clt() {
        return dtNasc_clt;
    }

    public void setDtNasc_clt(Date dtNasc_clt) {
        this.dtNasc_clt = dtNasc_clt;
    }

    public int getId_clt() {
        return id_clt;
    }

    public void setId_clt(int id_clt) {
        this.id_clt = id_clt;
    }

    public int getId_req() {
        return id_req;
    }

    public void setId_req(int id_req) {
        this.id_req = id_req;
    }

    public int getId_tax() {
        return id_tax;
    }

    public void setId_tax(int id_tax) {
        this.id_tax = id_tax;
    }

    public double getLat_req() {
        return lat_req;
    }

    public void setLat_req(double lat_req) {
        this.lat_req = lat_req;
    }

    public double getLng_req() {
        return lng_req;
    }

    public void setLng_req(double lng_req) {
        this.lng_req = lng_req;
    }
    
}
