package model;

public class Query1_1 {
    
    private int total_corr;
    private double dist_media;
    private double dist_min;
    private double dist_max;
    private double valor_media;
    private double valor_min;
    private double valor_max;
    private double valor_cpkr_media;
    private double valor_cpkr_min;
    private double valor_cpkr_max;

    public int getTotal_corr() {
        return total_corr;
    }

    public void setTotal_corr(int total_corr) {
        this.total_corr = total_corr;
    }

    public double getDist_media() {
        return dist_media;
    }

    public void setDist_media(double dist_media) {
        this.dist_media = dist_media;
    }

    public double getDist_min() {
        return dist_min;
    }

    public void setDist_min(double dist_min) {
        this.dist_min = dist_min;
    }

    public double getDist_max() {
        return dist_max;
    }

    public void setDist_max(double dist_max) {
        this.dist_max = dist_max;
    }

    public double getValor_media() {
        return valor_media;
    }

    public void setValor_media(double valor_media) {
        this.valor_media = valor_media;
    }

    public double getValor_min() {
        return valor_min;
    }

    public void setValor_min(double valor_min) {
        this.valor_min = valor_min;
    }

    public double getValor_max() {
        return valor_max;
    }

    public void setValor_max(double valor_max) {
        this.valor_max = valor_max;
    }

    public double getValor_cpkr_media() {
        return valor_cpkr_media;
    }

    public void setValor_cpkr_media(double valor_cpkr_media) {
        this.valor_cpkr_media = valor_cpkr_media;
    }

    public double getValor_cpkr_min() {
        return valor_cpkr_min;
    }

    public void setValor_cpkr_min(double valor_cpkr_min) {
        this.valor_cpkr_min = valor_cpkr_min;
    }

    public double getValor_cpkr_max() {
        return valor_cpkr_max;
    }

    public void setValor_cpkr_max(double valor_cpkr_max) {
        this.valor_cpkr_max = valor_cpkr_max;
    }
    
}
