package model;

public class Query2_1 {
    
    private int qtdade_total_corr_marca;
    private double valor_cpkr_media_marca;
    private double valor_cpkr_min_marca;
    private double valor_cpkr_max_marca;

    public int getQtdade_total_corr_marca() {
        return qtdade_total_corr_marca;
    }

    public void setQtdade_total_corr_marca(int qtdade_total_corr_marca) {
        this.qtdade_total_corr_marca = qtdade_total_corr_marca;
    }

    public double getValor_cpkr_media_marca() {
        return valor_cpkr_media_marca;
    }

    public void setValor_cpkr_media_marca(double valor_cpkr_media_marca) {
        this.valor_cpkr_media_marca = valor_cpkr_media_marca;
    }

    public double getValor_cpkr_min_marca() {
        return valor_cpkr_min_marca;
    }

    public void setValor_cpkr_min_marca(double valor_cpkr_min_marca) {
        this.valor_cpkr_min_marca = valor_cpkr_min_marca;
    }

    public double getValor_cpkr_max_marca() {
        return valor_cpkr_max_marca;
    }

    public void setValor_cpkr_max_marca(double valor_cpkr_max_marca) {
        this.valor_cpkr_max_marca = valor_cpkr_max_marca;
    }
    
}
