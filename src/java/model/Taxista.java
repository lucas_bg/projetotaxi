package model;

import java.math.BigDecimal;

public class Taxista extends Usuario{

    private BigDecimal valor_cpkr;

    public BigDecimal getValor_cpkr() {
        return valor_cpkr;
    }

    public void setValor_cpkr(BigDecimal valor_cpkr) {
        this.valor_cpkr = valor_cpkr;
    }
    
}
