package model;

public class Query3_3 {
    
    private String top_10_soma_valores_cliente_nome;
    private double top_10_soma_valores_cliente_soma;

    public String getTop_10_soma_valores_cliente_nome() {
        return top_10_soma_valores_cliente_nome;
    }

    public void setTop_10_soma_valores_cliente_nome(String top_10_soma_valores_cliente_nome) {
        this.top_10_soma_valores_cliente_nome = top_10_soma_valores_cliente_nome;
    }

    public double getTop_10_soma_valores_cliente_soma() {
        return top_10_soma_valores_cliente_soma;
    }

    public void setTop_10_soma_valores_cliente_soma(double top_10_soma_valores_cliente_soma) {
        this.top_10_soma_valores_cliente_soma = top_10_soma_valores_cliente_soma;
    }
    
}
