package model;

public class Historico2 {
    
    private double valor_medio;
    private int nota_media_do_clt;
    private double media_distancia;
    private int nota_media_do_tax;

    public double getValor_medio() {
        return valor_medio;
    }

    public void setValor_medio(double valor_medio) {
        this.valor_medio = valor_medio;
    }

    public int getNota_media_do_clt() {
        return nota_media_do_clt;
    }

    public void setNota_media_do_clt(int nota_media_do_clt) {
        this.nota_media_do_clt = nota_media_do_clt;
    }

    public double getMedia_distancia() {
        return media_distancia;
    }

    public void setMedia_distancia(double media_distancia) {
        this.media_distancia = media_distancia;
    }

    public int getNota_media_do_tax() {
        return nota_media_do_tax;
    }

    public void setNota_media_do_tax(int nota_media_do_tax) {
        this.nota_media_do_tax = nota_media_do_tax;
    }
    
}
