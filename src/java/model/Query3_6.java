package model;

public class Query3_6 {
    
    private String top_10_media_nota_taxista_nome;
    private double top_10_media_nota_taxista_media;

    public String getTop_10_media_nota_taxista_nome() {
        return top_10_media_nota_taxista_nome;
    }

    public void setTop_10_media_nota_taxista_nome(String top_10_media_nota_taxista_nome) {
        this.top_10_media_nota_taxista_nome = top_10_media_nota_taxista_nome;
    }

    public double getTop_10_media_nota_taxista_media() {
        return top_10_media_nota_taxista_media;
    }

    public void setTop_10_media_nota_taxista_media(double top_10_media_nota_taxista_media) {
        this.top_10_media_nota_taxista_media = top_10_media_nota_taxista_media;
    }
    
}
