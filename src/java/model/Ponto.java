package model;

import java.sql.Timestamp;

public class Ponto {
    
    private int id;
    private int id_tax;
    private int id_traj;
    private double lat;
    private double lng;
    private Timestamp pt;

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public int getId_tax() {
        return id_tax;
    }

    public void setId_tax(int id_tax) {
        this.id_tax = id_tax;
    }

    public int getId_traj() {
        return id_traj;
    }

    public void setId_traj(int id_traj) {
        this.id_traj = id_traj;
    }

    public double getLat() {
        return lat;
    }

    public void setLat(double lat) {
        this.lat = lat;
    }

    public double getLng() {
        return lng;
    }

    public void setLng(double lng) {
        this.lng = lng;
    }

    public Timestamp getPt() {
        return pt;
    }

    public void setPt(Timestamp pt) {
        this.pt = pt;
    }
    
}
