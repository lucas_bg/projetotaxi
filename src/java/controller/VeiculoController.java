package controller;

import dao.DAOFactory;
import dao.TaxistaDAO;
import dao.VeiculoDAO;
import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.math.BigDecimal;
import java.sql.SQLException;
import java.util.Base64;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.MultipartConfig;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import javax.servlet.http.Part;
import model.Taxista;
import model.Usuario;
import model.Veiculo;

@WebServlet(name = "VeiculoController",
        urlPatterns = {
            "/veiculo",
            "/veiculo/create",
            "/veiculo/update",
            "/veiculo/delete",
            "/veiculo/read"
        })
@MultipartConfig(maxFileSize = 16177215)

public class VeiculoController extends HttpServlet {

    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        
        RequestDispatcher requestDispatcher;
        
        switch (request.getServletPath()) {
            case "/veiculo":{
                //String url = "data:image/jpg;base64," + Base64.getEncoder().encodeToString(veiculo.getFotoAsByteArray());
                //request.setAttribute("urlFotoCarro", url);
                
                requestDispatcher = request.getRequestDispatcher("/view/veiculo/index.jsp");
                requestDispatcher.forward(request, response);
                break;
            }
            
            case "/veiculo/delete": {
                DAOFactory daoFactory;
                VeiculoDAO dao;
                try {
                    daoFactory = new DAOFactory();
                    dao = daoFactory.getVeiculoDAO();
                    
                    HttpSession sessao = request.getSession();
                    Usuario usuario = (Usuario) sessao.getAttribute("usuario");
                    
                    dao.deleteByUserId(usuario.getId());
                    
                    daoFactory.close();
                    
                    requestDispatcher = request.getRequestDispatcher("/view/veiculo/index.jsp");
                    requestDispatcher.forward(request, response);
                } catch (ClassNotFoundException | SQLException ex) {
                    Logger.getLogger(UsuarioController.class.getName()).log(Level.SEVERE, null, ex);
                }
                break;
            }
            
            case "/veiculo/create":{
                requestDispatcher = request.getRequestDispatcher("/view/veiculo/create.jsp");
                requestDispatcher.forward(request, response);
                break;
            }
            
            case "/veiculo/update":{
                //String url = "data:image/jpg;base64," + Base64.getEncoder().encodeToString(veiculo.getFotoAsByteArray());
                //request.setAttribute("urlFotoCarro", url);
                
                requestDispatcher = request.getRequestDispatcher("/view/veiculo/update.jsp");
                requestDispatcher.forward(request, response);
                break;
            }
            case "/veiculo/read": {
                Veiculo veiculo = null;
                InputStream inputStream;
                DAOFactory daoFactory;
                try {
                    daoFactory = new DAOFactory();
                    VeiculoDAO dao = daoFactory.getVeiculoDAO();

                    HttpSession sessao = request.getSession();
                    Usuario usuario = (Usuario) sessao.getAttribute("usuario");
                    
                    veiculo = dao.readUserId(usuario.getId());
                    
                    //SALVANDO IMAGEM PREPARADA
                    if(veiculo != null){
                        inputStream = veiculo.getFoto();
                        if(inputStream!=null){
                            try (ByteArrayOutputStream outputStream = new ByteArrayOutputStream()) {
                                int bytesRead;
                                byte[] buffer = new byte[1024];
                                while ((bytesRead = inputStream.read(buffer)) != -1) {
                                    outputStream.write(buffer, 0, bytesRead);
                                }

                                veiculo.setFotoAsByteArray(outputStream.toByteArray());

                                outputStream.close();
                            }
                            String url = "data:image/jpg;base64," + Base64.getEncoder().encodeToString(veiculo.getFotoAsByteArray());
                            request.setAttribute("urlFotoCarro", url);
                        }
                    }
                    //SALVANDO IMAGEM PREPARADA

                    daoFactory.close();
                } catch (ClassNotFoundException | SQLException ex) {
                    Logger.getLogger(UsuarioController.class.getName()).log(Level.SEVERE, null, ex);
                }
                
                request.setAttribute("veiculo", veiculo);
                requestDispatcher = request.getRequestDispatcher("/view/veiculo/read.jsp");
                requestDispatcher.forward(request, response);
                break;
            }
        }
    }

    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        
        DAOFactory daoFactory;
        VeiculoDAO dao;
            RequestDispatcher requestDispatcher;
        
        switch(request.getServletPath()){
            
            case "/veiculo/read":{
                Veiculo veiculo = null;
                InputStream inputStream;
                try {
                    daoFactory = new DAOFactory();
                    dao = daoFactory.getVeiculoDAO();
                    
                    veiculo = dao.readRenavam(request.getParameter("renavamPC"));
                    
                    //SALVANDO IMAGEM PREPARADA
                    if(veiculo != null){
                        inputStream = veiculo.getFoto();
                        try (ByteArrayOutputStream outputStream = new ByteArrayOutputStream()) {
                            int bytesRead;
                            byte[] buffer = new byte[1024];
                            while ((bytesRead = inputStream.read(buffer)) != -1) {
                                outputStream.write(buffer, 0, bytesRead);
                            }

                            veiculo.setFotoAsByteArray(outputStream.toByteArray());

                            outputStream.close();
                        }
                        String url = "data:image/jpg;base64," + Base64.getEncoder().encodeToString(veiculo.getFotoAsByteArray());
                        request.setAttribute("urlFotoCarro", url);
                    }
                    //SALVANDO IMAGEM PREPARADA

                    daoFactory.close();
                } catch (ClassNotFoundException | SQLException ex) {
                    Logger.getLogger(UsuarioController.class.getName()).log(Level.SEVERE, null, ex);
                }
                
                request.setAttribute("veiculo", veiculo);
                
                requestDispatcher = request.getRequestDispatcher("/view/veiculo/read.jsp");
                requestDispatcher.forward(request, response);
                break;
            }
            
            case "/veiculo/create":{
                Veiculo v = new Veiculo();
                Taxista taxista;
                
                v.setAno(request.getParameter("ano"));
                v.setRenavam(request.getParameter("renavam"));
                v.setCap(Integer.parseInt(request.getParameter("cap")));
                v.setCor(request.getParameter("cor"));
                v.setMarca(request.getParameter("marca"));
                v.setModelo(request.getParameter("modelo"));
                v.setPlaca(request.getParameter("placa"));
                
                String precoTemp = request.getParameter("preco");
                
                HttpSession sessao = request.getSession();
                Usuario usuario = (Usuario) sessao.getAttribute("usuario");
                //request.setAttribute("usuario", usuario);
                v.setId_usuario(usuario.getId());    
    
                InputStream inputStream = null;
                Part filePart = request.getPart("foto");
                if (filePart != null) {
                    inputStream = filePart.getInputStream();
                }
                if (inputStream != null) {
                    v.setFoto(inputStream);
                    v.setFotoSize(filePart.getSize());
                }
                
                try {
                    daoFactory = new DAOFactory();
                    dao = daoFactory.getVeiculoDAO();

                    dao.create(v);

                    TaxistaDAO daoTax = daoFactory.getTaxistaDAO();
                    taxista = daoTax.read(usuario.getId());

                    if(taxista!=null){
                        if(!precoTemp.equals("")){
                            taxista.setValor_cpkr(new BigDecimal(precoTemp));
                            taxista.setId(usuario.getId());
                            daoTax.update2(taxista);
                        }
                    } else {
                        taxista = new Taxista();
                        taxista.setValor_cpkr(new BigDecimal(precoTemp));
                        taxista.setId(usuario.getId());
                        daoTax.create(taxista);
                    }

                    daoFactory.close();

                    requestDispatcher = request.getRequestDispatcher("/view/veiculo/index.jsp");
                    requestDispatcher.forward(request, response);
                } catch (ClassNotFoundException | SQLException ex) {
                    Logger.getLogger(VeiculoController.class.getName()).log(Level.SEVERE, null, ex);
                }
            }
            
            case "/veiculo/update":{
                Veiculo v = new Veiculo();
                Taxista taxista;
                
                v.setAno(request.getParameter("ano"));
                v.setRenavam(request.getParameter("renavam"));
                v.setCap(Integer.parseInt(request.getParameter("cap")));
                v.setCor(request.getParameter("cor"));
                v.setMarca(request.getParameter("marca"));
                v.setModelo(request.getParameter("modelo"));
                v.setPlaca(request.getParameter("placa"));
                
                String precoTemp = request.getParameter("preco");
                
                HttpSession sessao = request.getSession();
                Usuario usuario = (Usuario) sessao.getAttribute("usuario");
                v.setId_usuario(usuario.getId());
                    
                InputStream inputStream = null;
                Part filePart = request.getPart("foto");
                if (filePart != null) {
                    inputStream = filePart.getInputStream();
                }
                if (inputStream != null) {
                    v.setFoto(inputStream);
                    v.setFotoSize(filePart.getSize());
                }
                
                try {
                    daoFactory = new DAOFactory();
                    dao = daoFactory.getVeiculoDAO();

                    dao.updateByUser(v);

                    TaxistaDAO daoTax = daoFactory.getTaxistaDAO();
                    taxista = daoTax.read(usuario.getId());

                    if(taxista!=null){
                        if(!precoTemp.equals("")){
                            taxista.setValor_cpkr(new BigDecimal(precoTemp));
                            taxista.setId(usuario.getId());
                            daoTax.update2(taxista);
                        }
                    } else {
                        taxista = new Taxista();
                        taxista.setValor_cpkr(new BigDecimal(precoTemp));
                        taxista.setId(usuario.getId());
                        daoTax.create(taxista);
                    }

                    daoFactory.close();

                    requestDispatcher = request.getRequestDispatcher("/view/veiculo/index.jsp");
                    requestDispatcher.forward(request, response);
                    
                } catch (ClassNotFoundException | SQLException ex) {
                    Logger.getLogger(VeiculoController.class.getName()).log(Level.SEVERE, null, ex);
                }
            }
        }
    }

    @Override
    public String getServletInfo() {
        return "Short description";
    }

}
