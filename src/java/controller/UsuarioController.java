package controller;

import EnvioPosicoes.Constantes;
import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import dao.DAOFactory;
import dao.EstGlobDAO;
import dao.HistoricoDAO;
import dao.PontoDAO;
import dao.ReqCorridaDAO;
import dao.TaxistaDAO;
import dao.UsuarioDAO;
import dao.VeiculoDAO;
import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.PrintWriter;
import java.math.BigDecimal;
import java.sql.SQLException;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Base64;
import java.util.Date;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.MultipartConfig;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import javax.servlet.http.Part;
import model.FindAndHead;
import model.Historico;
import model.Historico2;
import model.Ponto;
import model.Query1_1;
import model.Query2_1;
import model.Query2_2;
import model.Query3_1;
import model.Query3_2;
import model.Query3_3;
import model.Query3_4;
import model.Query3_5;
import model.Query3_6;
import model.Query4_1;
import model.ReqCorrida;
import model.Taxista;
import model.Usuario;
import model.ViewReqsTaxista;

@WebServlet(name = "UsuarioController",
        urlPatterns = {
            "/usuario",
            "/usuario/create",
            "/usuario/update",
            "/usuario/delete",
            "/usuario/read",
            "/usuario/reqCorr",
            "/usuario/escolheuPonto",
            "/usuario/escolheuCorrida",
            "/usuario/verifAct",
            "/usuario/verifAct2",
            "/usuario/historico",
            "/usuario/estGlob",
            "/usuario/taxVerifPorReqs",
            "/usuario/taxVerifCorrAComecar"
        })
@MultipartConfig(maxFileSize = 16177215)

public class UsuarioController extends HttpServlet {
    
    public static String formatDatee (String date, String initDateFormat, String endDateFormat) throws ParseException {

        Date initDate = new SimpleDateFormat(initDateFormat).parse(date);
        SimpleDateFormat formatter = new SimpleDateFormat(endDateFormat);
        String parsedDate = formatter.format(initDate);

        return parsedDate;
    }
    
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        
        RequestDispatcher requestDispatcher;

        switch (request.getServletPath()) {
            case "/usuario": {
                HttpSession sessao = request.getSession();
                Usuario usuario = (Usuario) sessao.getAttribute("usuario");
                String url = "data:image/jpg;base64," + Base64.getEncoder().encodeToString(usuario.getFotoAsByteArray());
                request.setAttribute("urlll", url);
                List<ViewReqsTaxista> lista = null;
                List<ViewReqsTaxista> listaCorr = null;
                
                if((boolean)sessao.getAttribute("isTaxista")){
                    DAOFactory daoFactory;
                    TaxistaDAO dao;
                    try {
                        daoFactory = new DAOFactory();
                        dao = daoFactory.getTaxistaDAO();
                        
                        lista = dao.verReqCorridasByTaxId(usuario.getId());
                        
                        listaCorr = dao.verCorridasByTaxId(usuario.getId());
                        
                        daoFactory.close();
                    } catch (ClassNotFoundException | SQLException ex) {
                        Logger.getLogger(UsuarioController.class.getName()).log(Level.SEVERE, null, ex);
                    }
                }
                request.setAttribute("listaCorrEspPIniciar", listaCorr);
                request.setAttribute("listaReqsPTaxEsp", lista);
                requestDispatcher = request.getRequestDispatcher("/view/usuario/index.jsp");
                requestDispatcher.forward(request, response);
                break;
            }
            case "/usuario/taxVerifPorReqs": {
                HttpSession sessao = request.getSession();
                Usuario usuario = (Usuario) sessao.getAttribute("usuario");
                List<ViewReqsTaxista> listaReqsPTaxEsp = null;
                DAOFactory daoFactory;
                TaxistaDAO dao;
                try {
                    daoFactory = new DAOFactory();
                    dao = daoFactory.getTaxistaDAO();

                    listaReqsPTaxEsp = dao.verReqCorridasByTaxId(usuario.getId());

                    daoFactory.close();
                } catch (ClassNotFoundException | SQLException ex) {
                    Logger.getLogger(UsuarioController.class.getName()).log(Level.SEVERE, null, ex);
                }
                
                GsonBuilder builder = new GsonBuilder();
                builder.setDateFormat("dd-MM-yyyy hh:mm:ss.S");
                Gson gson = builder.create();

                response.setContentType("application/json");
                response.setCharacterEncoding("utf-8");
                PrintWriter out = response.getWriter();

                out.print(gson.toJson(listaReqsPTaxEsp));
                break;
            }
            case "/usuario/taxVerifCorrAComecar": {
                HttpSession sessao = request.getSession();
                Usuario usuario = (Usuario) sessao.getAttribute("usuario");
                List<ViewReqsTaxista> listaCorr = null;
                DAOFactory daoFactory;
                TaxistaDAO dao;
                try {
                    daoFactory = new DAOFactory();
                    dao = daoFactory.getTaxistaDAO();

                    listaCorr = dao.verCorridasByTaxId(usuario.getId());

                    daoFactory.close();
                } catch (ClassNotFoundException | SQLException ex) {
                    Logger.getLogger(UsuarioController.class.getName()).log(Level.SEVERE, null, ex);
                }
                
                GsonBuilder builder = new GsonBuilder();
                builder.setDateFormat("dd-MM-yyyy hh:mm:ss.S");
                Gson gson = builder.create();

                response.setContentType("application/json");
                response.setCharacterEncoding("utf-8");
                PrintWriter out = response.getWriter();

                out.print(gson.toJson(listaCorr));
                break;
            }
            case "/usuario/verifAct": {
                HttpSession session = request.getSession();
                ReqCorrida rc = (ReqCorrida) session.getAttribute("reqAtual");
                
                DAOFactory daoFactory;
                ReqCorridaDAO dao;
                try {
                    daoFactory = new DAOFactory();
                    dao = daoFactory.getReqCorridaDAO();
                    
                    ReqCorrida newRc = dao.read(rc.getId());
                    if(newRc.isAnalisada()){
                        session.setAttribute("waitResp", false);
                        if(newRc.isAceita()){
                            session.setAttribute("corrAceita", true);
                        }else {
                            session.setAttribute("corrRecusada", true);
                        }
                    }else {
                        session.setAttribute("waitResp", true);
                    }
                    session.setAttribute("reqAtual", newRc);
                    
                    daoFactory.close();
                } catch (ClassNotFoundException | SQLException ex) {
                    Logger.getLogger(UsuarioController.class.getName()).log(Level.SEVERE, null, ex);
                }
                requestDispatcher = request.getRequestDispatcher("/view/usuario/index.jsp");
                requestDispatcher.forward(request, response);
                break;
            }
            case "/usuario/escolheuPonto": {
                int id = Integer.parseInt(request.getParameter("id"));
                HttpSession session = request.getSession();
                List<FindAndHead> fahList = (ArrayList < FindAndHead > ) session.getAttribute("fahList");
                FindAndHead fahEl = fahList.get(id);
                Ponto headEsc = fahEl.getHead();
                Ponto findEsc = fahEl.getFind();
                DAOFactory daoFactory;
                ReqCorridaDAO dao;
                
                try {
                    daoFactory = new DAOFactory();
                    dao = daoFactory.getReqCorridaDAO();

                    ReqCorrida rc = new ReqCorrida();
                    rc.setTraj_esc(headEsc.getId_traj());
                    rc.setId_p0(headEsc.getId());
                    rc.setId_pi(findEsc.getId());
                    rc.setId_clt(((Usuario)session.getAttribute("usuario")).getId());
                    rc.setId_tax(headEsc.getId_tax());
                    dao.create(rc);
                    session.setAttribute("reqAtual", rc);
                    
                    daoFactory.close();
                } catch (ClassNotFoundException | SQLException ex) {
                    Logger.getLogger(UsuarioController.class.getName()).log(Level.SEVERE, null, ex);
                }

                session.setAttribute("waitResp", true);
                requestDispatcher = request.getRequestDispatcher("/view/usuario/index.jsp");
                requestDispatcher.forward(request, response);
                break;
            }
            case "/usuario/escolheuCorrida": {
                int id = Integer.parseInt(request.getParameter("id"));
                boolean op = Boolean.parseBoolean(request.getParameter("esc"));
                DAOFactory daoFactory;
                TaxistaDAO dao;
                try {
                    daoFactory = new DAOFactory();
                    dao = daoFactory.getTaxistaDAO();

                    dao.aceitarOuRecusarCorrida(op, id);

                    daoFactory.close();
                } catch (ClassNotFoundException | SQLException ex) {
                    Logger.getLogger(UsuarioController.class.getName()).log(Level.SEVERE, null, ex);
                }
                
                requestDispatcher = request.getRequestDispatcher("/view/usuario/index.jsp");
                requestDispatcher.forward(request, response);
                break;
            }
            case "/usuario/read": {
                Usuario usuario = null;
                InputStream inputStream;
                DAOFactory daoFactory;
                HttpSession sessao = request.getSession();
                Usuario usuarioR = (Usuario) sessao.getAttribute("usuario");
                try {
                    daoFactory = new DAOFactory();
                    UsuarioDAO dao = daoFactory.getUsuarioDAO();

                    usuario = dao.read(usuarioR.getId());
                    
                    //SALVANDO IMAGEM PREPARADA
                    if(usuario != null){
                        inputStream = usuario.getFoto();
                        try (ByteArrayOutputStream outputStream = new ByteArrayOutputStream()) {
                            int bytesRead;
                            byte[] buffer = new byte[1024];
                            while ((bytesRead = inputStream.read(buffer)) != -1) {
                                outputStream.write(buffer, 0, bytesRead);
                            }

                            usuario.setFotoAsByteArray(outputStream.toByteArray());

                            outputStream.close();
                        }
                        String url = "data:image/jpg;base64," + Base64.getEncoder().encodeToString(usuario.getFotoAsByteArray());
                        request.setAttribute("urlll", url);
                    }
                    //SALVANDO IMAGEM PREPARADA

                    daoFactory.close();
                } catch (ClassNotFoundException | SQLException ex) {
                    Logger.getLogger(UsuarioController.class.getName()).log(Level.SEVERE, null, ex);
                }
                
                try {
                    daoFactory = new DAOFactory();
                    TaxistaDAO daoT = daoFactory.getTaxistaDAO();
                    Taxista taxista;

                    taxista = daoT.read(usuarioR.getId());
                    if(taxista==null){
                        sessao.setAttribute("ehTaxista", false);
                    }else{
                        sessao.setAttribute("ehTaxista", true);
                        request.setAttribute("taxista", taxista);
                    }
                    daoFactory.close();
                } catch (ClassNotFoundException | SQLException ex) {
                    Logger.getLogger(UsuarioController.class.getName()).log(Level.SEVERE, null, ex);
                }
                
                sessao.setAttribute("usuario", usuario);

                requestDispatcher = request.getRequestDispatcher("/view/usuario/read.jsp");
                requestDispatcher.forward(request, response);
                break;
            }

            case "/usuario/create": {
                requestDispatcher = request.getRequestDispatcher("/view/usuario/create.jsp");
                requestDispatcher.forward(request, response);
                break;
            }

            case "/usuario/update": {
                HttpSession sessao = request.getSession();
                Usuario usuario = (Usuario) sessao.getAttribute("usuario");
                request.setAttribute("usuario", usuario);
                String url = "data:image/jpg;base64," + Base64.getEncoder().encodeToString(usuario.getFotoAsByteArray());
                request.setAttribute("urlll", url);
                
                DAOFactory daoFactory;
                try {
                    daoFactory = new DAOFactory();
                    TaxistaDAO daoT = daoFactory.getTaxistaDAO();
                    Taxista taxista;

                    taxista = daoT.read(usuario.getId());
                    if(taxista==null){
                        sessao.setAttribute("ehTaxista", false);
                    }else{
                        sessao.setAttribute("ehTaxista", true);
                    }
                    daoFactory.close();
                } catch (ClassNotFoundException | SQLException ex) {
                    Logger.getLogger(UsuarioController.class.getName()).log(Level.SEVERE, null, ex);
                }

                requestDispatcher = request.getRequestDispatcher("/view/usuario/update.jsp");
                requestDispatcher.forward(request, response);
                break;
            }
            case "/usuario/delete": {
                DAOFactory daoFactory;
                HttpSession session = request.getSession();
                Usuario user = (Usuario) session.getAttribute("usuario");
                try {
                    daoFactory = new DAOFactory();
                    VeiculoDAO dao = daoFactory.getVeiculoDAO();
                    
                    dao.deleteByUserId(user.getId());
                    daoFactory.close();
                } catch (ClassNotFoundException | SQLException ex) {
                    Logger.getLogger(UsuarioController.class.getName()).log(Level.SEVERE, null, ex);
                }
                try {
                    daoFactory = new DAOFactory();
                    TaxistaDAO dao2 = daoFactory.getTaxistaDAO();
                    
                    dao2.delete(user.getId());
                    daoFactory.close();
                } catch (ClassNotFoundException | SQLException ex) {
                    Logger.getLogger(UsuarioController.class.getName()).log(Level.SEVERE, null, ex);
                }
                response.sendRedirect(request.getContextPath() + "/logout");
                break;
            }
            case "/usuario/reqCorr": {
                List<Ponto> lista = null;
                PontoDAO dao;
                DAOFactory daoFactory;
                
                try {
                    daoFactory = new DAOFactory();
                    dao = daoFactory.getPontoDAO();
                    
                    double latAtualClt = Double.valueOf(request.getParameter("lat"));
                    double lngAtualClt = Double.valueOf(request.getParameter("lng"));
                    
                    HttpSession sessao = request.getSession();
                    sessao.setAttribute("latAtualClt", latAtualClt);
                    sessao.setAttribute("lngAtualClt", lngAtualClt);

                    lista = dao.searchNextQuery(latAtualClt, lngAtualClt);
                    
                    daoFactory.close();
                } catch (ClassNotFoundException | SQLException ex) {
                    Logger.getLogger(UsuarioController.class.getName()).log(Level.SEVERE, null, ex);
                }
                List<FindAndHead> fahList = new ArrayList<>();
                FindAndHead fahEl = null;
                int i = 0;
                
                for (Ponto el : lista) {
                    fahEl = new FindAndHead();
                    try {
                        daoFactory = new DAOFactory();
                        dao = daoFactory.getPontoDAO();
                        
                        Ponto head = dao.headByFoundId(el.getId_traj());
                        fahEl.setFind(el);
                        fahEl.setHead(head);
                        fahEl.setI(i);
                        
                        fahList.add(i, fahEl);
                        i++;
                        daoFactory.close();
                    } catch (ClassNotFoundException | SQLException ex) {
                        Logger.getLogger(UsuarioController.class.getName()).log(Level.SEVERE, null, ex);
                    }
                }
                
                HttpSession session = request.getSession();
                session.setAttribute("fahList", fahList);

                // Instanciando conversor de objetos java para JSON
                GsonBuilder builder = new GsonBuilder();
                builder.setDateFormat("dd-MM-yyyy hh:mm:ss.S");
                Gson gson = builder.create();

                // Preparar resposta JSON
                response.setContentType("application/json");
                response.setCharacterEncoding("utf-8");
                PrintWriter out = response.getWriter();

                // Conversão JSON e escrita da resposta
                out.print(gson.toJson(fahList));
                break;
            }
            case "/usuario/historico": {
                
                
                requestDispatcher = request.getRequestDispatcher("/view/usuario/hist.jsp");
                requestDispatcher.forward(request, response);
                break;
            }
            case "/usuario/estGlob": {
                
                
                requestDispatcher = request.getRequestDispatcher("/view/usuario/estGlob.jsp");
                requestDispatcher.forward(request, response);
                break;
            }
        }
    }
    
    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {

        DAOFactory daoFactory;
        
        switch (request.getServletPath()) {
            case "/usuario/reqCorr": {
                List<Ponto> lista = null;
                PontoDAO dao;
                
                try {
                    daoFactory = new DAOFactory();
                    dao = daoFactory.getPontoDAO();
                    
                    double latAtualClt = Double.valueOf(request.getParameter("lat"));
                    double lngAtualClt = Double.valueOf(request.getParameter("lng"));
                    
                    HttpSession sessao = request.getSession();
                    sessao.setAttribute("latAtualClt", latAtualClt);
                    sessao.setAttribute("lngAtualClt", lngAtualClt);

                    lista = dao.searchNextQuery(latAtualClt, lngAtualClt);
                    
                    daoFactory.close();
                } catch (ClassNotFoundException | SQLException ex) {
                    Logger.getLogger(UsuarioController.class.getName()).log(Level.SEVERE, null, ex);
                }
                List<FindAndHead> fahList = new ArrayList<>();
                FindAndHead fahEl;
                int i = 0;
                
                for (Ponto el : lista) {
                    fahEl = new FindAndHead();
                    try {
                        daoFactory = new DAOFactory();
                        dao = daoFactory.getPontoDAO();
                        
                        Ponto head = dao.headByFoundId(el.getId_traj());
                        fahEl.setFind(el);
                        fahEl.setHead(head);
                        fahEl.setI(i);
                        
                        fahList.add(i, fahEl);
                        i++;
                        daoFactory.close();
                    } catch (ClassNotFoundException | SQLException ex) {
                        Logger.getLogger(UsuarioController.class.getName()).log(Level.SEVERE, null, ex);
                    }
                }
                
                HttpSession session = request.getSession();
                session.setAttribute("fahList", fahList);
                RequestDispatcher requestDispatcher = request.getRequestDispatcher("/view/usuario/index.jsp");
                requestDispatcher.forward(request, response);
                break;
            }
            case "/usuario/create": {
                Usuario usuario = new Usuario();
                UsuarioDAO dao;

                usuario.setLogin(request.getParameter("login"));
                usuario.setSenha(request.getParameter("senha"));
                usuario.setPnome(request.getParameter("pnome"));
                usuario.setSnome(request.getParameter("snome"));
                usuario.setTel(request.getParameter("tel"));
                usuario.setSexo(request.getParameter("sexo"));
                usuario.setCpf(request.getParameter("cpf"));
                usuario.setAtivo(true);
                
                InputStream inputStream = null;
                Part filePart = request.getPart("foto");
                if (filePart != null) {
                    inputStream = filePart.getInputStream();
                }
                if (inputStream != null) {
                    usuario.setFoto(inputStream);
                    usuario.setFotoSize(filePart.getSize());
                }

                SimpleDateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd");

                try {
                    Date dataNascimento = dateFormat.parse(request.getParameter("dataNasc"));
                    java.sql.Date dataa = new java.sql.Date(dataNascimento.getTime());
                    usuario.setDtNasc(dataa);

                    daoFactory = new DAOFactory();

                    dao = daoFactory.getUsuarioDAO();

                    dao.create(usuario);
                    daoFactory.close();

                    response.sendRedirect(request.getContextPath() + "/login");
                } catch (ParseException ex) {
                    System.err.println(ex.getMessage());
                } catch (ClassNotFoundException | SQLException ex) {
                    Logger.getLogger(UsuarioController.class.getName()).log(Level.SEVERE, null, ex);
                }
                break;
            }
            case "/usuario/update": {
                Usuario usuario = new Usuario();
                UsuarioDAO dao;

                usuario.setId(Integer.parseInt(request.getParameter("id")));
                usuario.setLogin(request.getParameter("login"));
                usuario.setSenha(request.getParameter("senha"));
                usuario.setPnome(request.getParameter("pnome"));
                usuario.setSnome(request.getParameter("snome"));
                usuario.setTel(request.getParameter("tel"));
                usuario.setSexo(request.getParameter("sexo"));
                usuario.setCpf(request.getParameter("cpf"));
                usuario.setAtivo(true);
                
                InputStream inputStream = null;
                Part filePart = request.getPart("foto");
                if (filePart != null) {
                    inputStream = filePart.getInputStream();
                }
                if (inputStream != null) {
                    usuario.setFoto(inputStream);
                    usuario.setFotoSize(filePart.getSize());
                }
                
                SimpleDateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd");

                try {
                    Date dataNascimento = dateFormat.parse(request.getParameter("dataNasc"));
                    java.sql.Date dataa = new java.sql.Date(dataNascimento.getTime());
                    usuario.setDtNasc(dataa);

                    daoFactory = new DAOFactory();

                    dao = daoFactory.getUsuarioDAO();
                    dao.update(usuario);
                    daoFactory.close();

                } catch (ParseException ex) {
                    System.err.println(ex.getMessage());
                } catch (ClassNotFoundException | SQLException ex) {
                    Logger.getLogger(UsuarioController.class.getName()).log(Level.SEVERE, null, ex);
                }
                
                HttpSession sessao = request.getSession();
                boolean ehTaxista = (Boolean) sessao.getAttribute("ehTaxista");
                if(ehTaxista){
                    try {
                        daoFactory = new DAOFactory();
                        Taxista taxista = new Taxista();
                        TaxistaDAO daoT = daoFactory.getTaxistaDAO();
                        taxista.setId(Integer.parseInt(request.getParameter("id")));
                        taxista.setValor_cpkr(new BigDecimal(request.getParameter("valor")));
                        daoT.update2(taxista);
                        daoFactory.close();
                    } catch (ClassNotFoundException | SQLException ex) {
                        Logger.getLogger(UsuarioController.class.getName()).log(Level.SEVERE, null, ex);
                    }
                }
                
                response.sendRedirect(request.getContextPath() + "/usuario");
                break;
            }
            case "/usuario/historico": {
                String dataRecebidaInicial = request.getParameter("dataInicial");
                String dataRecebidaFinal = request.getParameter("dataFinal");
                java.sql.Date dataParaSQLInicial;
                java.sql.Date dataParaSQLFinal;
                String formatoEntrada = "dd/MM/yyyy";
                String formatoSaida = "yyyy-MM-dd";
                HttpSession session = request.getSession();
                Usuario user = (Usuario) session.getAttribute("usuario");
                
                if(dataRecebidaInicial.equals("")){
                    dataParaSQLInicial = Constantes.getInitialDate();
                }else {
                    SimpleDateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd");
                    Date dataaaa = null;
                    try {
                        dataRecebidaInicial = formatDatee(dataRecebidaInicial, formatoEntrada, formatoSaida);
                    } catch (ParseException ex) {
                        Logger.getLogger(UsuarioController.class.getName()).log(Level.SEVERE, null, ex);
                    }
                    try {
                        dataaaa = dateFormat.parse(dataRecebidaInicial);
                    } catch (ParseException ex) {
                        Logger.getLogger(UsuarioController.class.getName()).log(Level.SEVERE, null, ex);
                    }
                    dataParaSQLInicial = new java.sql.Date(dataaaa.getTime());
                }
                
                if(dataRecebidaFinal.equals("")){
                    dataParaSQLFinal = Constantes.getTodaysDate();
                }else {
                    SimpleDateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd");
                    Date dataaaa = null;
                    try {
                        dataRecebidaFinal = formatDatee(dataRecebidaFinal, formatoEntrada, formatoSaida);
                    } catch (ParseException ex) {
                        Logger.getLogger(UsuarioController.class.getName()).log(Level.SEVERE, null, ex);
                    }
                    try {
                        dataaaa = dateFormat.parse(dataRecebidaFinal);
                    } catch (ParseException ex) {
                        Logger.getLogger(UsuarioController.class.getName()).log(Level.SEVERE, null, ex);
                    }
                    dataParaSQLFinal = new java.sql.Date(dataaaa.getTime());
                }
                                
                HistoricoDAO dao;
                List<Historico> lista = null;
                Historico2 medias = null;
                try {
                    daoFactory = new DAOFactory();
                    dao = daoFactory.getHistoricoDAO();
                    
                    if((boolean) session.getAttribute("isTaxista")){
                        lista = dao.readForTaxLista(user.getId(), dataParaSQLInicial, dataParaSQLFinal);
                        medias = dao.readForTaxMedias(user.getId(), dataParaSQLInicial, dataParaSQLFinal);
                    } else {
                        lista = dao.readForCltLista(user.getId(), dataParaSQLInicial, dataParaSQLFinal);
                        medias = dao.readForCltMedias(user.getId(), dataParaSQLInicial, dataParaSQLFinal);
                    }
                    
                    daoFactory.close();
                } catch (ClassNotFoundException | SQLException ex) {
                    Logger.getLogger(UsuarioController.class.getName()).log(Level.SEVERE, null, ex);
                }
                
                session.setAttribute("listaHistoricos", lista);
                session.setAttribute("mediasHistCorr", medias);

                RequestDispatcher requestDispatcher = request.getRequestDispatcher("/view/usuario/hist.jsp");
                requestDispatcher.forward(request, response);
                break;
            }
            case "/usuario/estGlob": {
                String dataRecebidaInicial = request.getParameter("dataInicial");
                String dataRecebidaFinal = request.getParameter("dataFinal");
                java.sql.Date dataParaSQLInicial;
                java.sql.Date dataParaSQLFinal;
                String formatoEntrada = "dd/MM/yyyy";
                String formatoSaida = "yyyy-MM-dd";
                HttpSession session = request.getSession();
                
                if(dataRecebidaInicial.equals("")){
                    dataParaSQLInicial = Constantes.getInitialDate();
                }else {
                    SimpleDateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd");
                    Date dataaaa = null;
                    try {
                        dataRecebidaInicial = formatDatee(dataRecebidaInicial, formatoEntrada, formatoSaida);
                    } catch (ParseException ex) {
                        Logger.getLogger(UsuarioController.class.getName()).log(Level.SEVERE, null, ex);
                    }
                    try {
                        dataaaa = dateFormat.parse(dataRecebidaInicial);
                    } catch (ParseException ex) {
                        Logger.getLogger(UsuarioController.class.getName()).log(Level.SEVERE, null, ex);
                    }
                    dataParaSQLInicial = new java.sql.Date(dataaaa.getTime());
                }
                
                if(dataRecebidaFinal.equals("")){
                    dataParaSQLFinal = Constantes.getTodaysDate();
                }else {
                    SimpleDateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd");
                    Date dataaaa = null;
                    try {
                        dataRecebidaFinal = formatDatee(dataRecebidaFinal, formatoEntrada, formatoSaida);
                    } catch (ParseException ex) {
                        Logger.getLogger(UsuarioController.class.getName()).log(Level.SEVERE, null, ex);
                    }
                    try {
                        dataaaa = dateFormat.parse(dataRecebidaFinal);
                    } catch (ParseException ex) {
                        Logger.getLogger(UsuarioController.class.getName()).log(Level.SEVERE, null, ex);
                    }
                    dataParaSQLFinal = new java.sql.Date(dataaaa.getTime());
                }
                
                java.sql.Date d1 = dataParaSQLInicial;
                java.sql.Date d2 = dataParaSQLFinal;
                
                EstGlobDAO dao;
                Query1_1 resultQuery1_1 = null;
                List<Query2_1> listaQuery2_1 = null;
                List<Query2_2> listaQuery2_2 = null;
                List<Query3_1> listaQuery3_1 = null;
                List<Query3_2> listaQuery3_2 = null;
                List<Query3_3> listaQuery3_3 = null;
                List<Query3_4> listaQuery3_4 = null;
                List<Query3_5> listaQuery3_5 = null;
                List<Query3_6> listaQuery3_6 = null;
                List<Query4_1> listaQuery4_1 = null;
                try {
                    daoFactory = new DAOFactory();
                    dao = daoFactory.getEstGlobDAO();
                    
                    resultQuery1_1 = dao.funcQuery1_1(d1, d2);
                    listaQuery2_1 = dao.funcQuery2_1(d1, d2);
                    listaQuery2_2 = dao.funcQuery2_2(d1, d2);
                    listaQuery3_1 = dao.funcQuery3_1(d1, d2);
                    listaQuery3_2 = dao.funcQuery3_2(d1, d2);
                    listaQuery3_3 = dao.funcQuery3_3(d1, d2);
                    listaQuery3_4 = dao.funcQuery3_4(d1, d2);
                    listaQuery3_5 = dao.funcQuery3_5(d1, d2);
                    listaQuery3_6 = dao.funcQuery3_6(d1, d2);
                    listaQuery4_1 = dao.funcQuery4_1(d1, d2);
                    
                    daoFactory.close();
                } catch (ClassNotFoundException | SQLException ex) {
                    Logger.getLogger(UsuarioController.class.getName()).log(Level.SEVERE, null, ex);
                }
                
                session.setAttribute("query1_1", resultQuery1_1);
                session.setAttribute("query2_1", listaQuery2_1);
                session.setAttribute("query2_2", listaQuery2_2);
                session.setAttribute("query3_1", listaQuery3_1);
                session.setAttribute("query3_2", listaQuery3_2);
                session.setAttribute("query3_3", listaQuery3_3);
                session.setAttribute("query3_4", listaQuery3_4);
                session.setAttribute("query3_5", listaQuery3_5);
                session.setAttribute("query3_6", listaQuery3_6);
                session.setAttribute("query4_1", listaQuery4_1);
                
                RequestDispatcher requestDispatcher = request.getRequestDispatcher("/view/usuario/estGlob.jsp");
                requestDispatcher.forward(request, response);
                break;
            }
        }
    }
    
    @Override
    public String getServletInfo() {
        return "Short description";
    }
    
}
