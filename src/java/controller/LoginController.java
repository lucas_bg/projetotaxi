package controller;

import dao.DAOFactory;
import dao.TaxistaDAO;
import dao.UsuarioDAO;
import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.sql.SQLException;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import model.Taxista;
import model.Usuario;

@WebServlet(name = "LoginController", urlPatterns = {"/login", "/logout"})
public class LoginController extends HttpServlet {
    
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        switch (request.getServletPath()) {
            case "/logout": {
                HttpSession session = request.getSession(false);
                session.invalidate();

                response.sendRedirect(request.getContextPath() + "/");
                break;
            }
            case "/login": {
                RequestDispatcher requestDispatcher = request.getRequestDispatcher("/view/login/login.jsp");
                requestDispatcher.forward(request, response);
                break;
            }
        }
    }
    
    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        
        DAOFactory daoFactory;
        UsuarioDAO dao;
        InputStream inputStream;
        
        if (request.getServletPath().equals("/login")) {
            try {
                daoFactory = new DAOFactory();
                dao = daoFactory.getUsuarioDAO();

                Usuario usuario = dao.authenticate(
                        request.getParameter("login"),
                        request.getParameter("senha"));
                
                //SALVANDO IMAGEM PREPARADA
                if(usuario != null){
                    inputStream = usuario.getFoto();
                    try (ByteArrayOutputStream outputStream = new ByteArrayOutputStream()) {
                        int bytesRead;
                        byte[] buffer = new byte[1024];
                        while ((bytesRead = inputStream.read(buffer)) != -1) {
                            outputStream.write(buffer, 0, bytesRead);
                        }

                        usuario.setFotoAsByteArray(outputStream.toByteArray());

                        outputStream.close();
                    }
                }
                //SALVANDO IMAGEM PREPARADA
                
                TaxistaDAO daoTax = daoFactory.getTaxistaDAO();
                
                if (usuario != null) {
                    Taxista taxista = daoTax.read(usuario.getId());
                    HttpSession sessao = request.getSession(true);
                    sessao.setMaxInactiveInterval(600);
                    sessao.setAttribute("usuario", usuario);
                    if(taxista != null){
                        sessao.setAttribute("isTaxista", true);
                    }else{
                        sessao.setAttribute("isTaxista", false);
                    }
                    
                    response.sendRedirect(request.getContextPath() + "/usuario");
                } else {
                    RequestDispatcher requestDispatcher = request.getRequestDispatcher("/index.jsp");
                    requestDispatcher.forward(request, response);
                }
                daoFactory.close();
            } catch (ClassNotFoundException | SQLException ex) {
                Logger.getLogger(LoginController.class.getName()).log(Level.SEVERE, null, ex);
            }
        }
    }
    
    @Override
    public String getServletInfo() {
        return "Short description";
    }
    
}
