package controller;

import EnvioPosicoes.Constantes;
import EnvioPosicoes.Enviador;
import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import dao.CorridaDAO;
import dao.DAOFactory;
import dao.PontoDAO;
import dao.ReqCorridaDAO;
import dao.TaxistaDAO;
import java.io.IOException;
import java.io.PrintWriter;
import java.math.BigDecimal;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import model.Corrida;
import model.Ponto;
import model.ReqCorrida;
import model.Taxista;

@WebServlet(name = "CorridaController", 
        urlPatterns = {
            "/corrida",
            "/corrida/comeca",
            "/corrida/atualizar",
            "/corrida/atualizar2",
            "/corrida/encontro",
            "/corrida/escolheuDestino",
            "/corrida/aguardandoTax",
            "/corrida/comecouCorridapt2",
            "/corrida/visualCorrPt2",
            "/corrida/fim",
            "/corrida/taxistaAguFim",
            "/corrida/notaCliente",
            "/corrida/notaTaxista",
            "/corrida/encontroJS",
            "/corrida/atualizaDesloc",
            "/corrida/atualizaDesloc2"
        })

public class CorridaController extends HttpServlet {

    Enviador env = null;
    Enviador env2 = null;
    List<Ponto> listaPtsTrajPt2 = null;
    Ponto finalPoint = null;
    
    public static double distFrom(double lat1, double lng1, double lat2, double lng2) {
        double earthRadius = 6371.0; // miles (or 6371.0 kilometers)
        double dLat = Math.toRadians(lat2 - lat1);
        double dLng = Math.toRadians(lng2 - lng1);
        double sindLat = Math.sin(dLat / 2);
        double sindLng = Math.sin(dLng / 2);
        double a = Math.pow(sindLat, 2) + Math.pow(sindLng, 2)
                * Math.cos(Math.toRadians(lat1)) * Math.cos(Math.toRadians(lat2));
        double c = 2 * Math.atan2(Math.sqrt(a), Math.sqrt(1 - a));
        double dist = earthRadius * c;
        return dist;
    }
    
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        RequestDispatcher requestDispatcher;
        DAOFactory daoFactory;
        
        switch (request.getServletPath()) {
            case "/corrida": {
                
                requestDispatcher = request.getRequestDispatcher("/view/corrida/index.jsp");
                requestDispatcher.forward(request, response);
                break;
            }
            case "/corrida/comeca": {
                HttpSession session = request.getSession();
                ReqCorrida rc = (ReqCorrida) session.getAttribute("reqAtual");
                PontoDAO dao;
                List<Ponto> listaPtsTraj = new ArrayList<>();
                
                try {
                    daoFactory = new DAOFactory();
                    dao = daoFactory.getPontoDAO();
                    
                    listaPtsTraj = dao.allPointsById_traj(rc.getTraj_esc());
                    
                    daoFactory.close();
                } catch (ClassNotFoundException | SQLException ex) {
                    Logger.getLogger(CorridaController.class.getName()).log(Level.SEVERE, null, ex);
                }
                
                env = new Enviador(listaPtsTraj, rc.getId_pi());
                Thread t = new Thread(env);
                t.start();
                
                session.setAttribute("corrAceita", false);
                session.setAttribute("listaPtsTraj", listaPtsTraj);
                request.setAttribute("pontoAtualDesloc", listaPtsTraj.get(0));
                requestDispatcher = request.getRequestDispatcher("/view/corrida/index.jsp");
                requestDispatcher.forward(request, response);
                break;
            }
            case "/corrida/encontro": {
                int meetPoint = env.getFinalIndex();
                HttpSession session = request.getSession();
                List<Ponto> lista = (ArrayList<Ponto>) session.getAttribute("listaPtsTraj");
                List<Ponto> trajPt2 = new ArrayList<>();
                
                for(int i = meetPoint + 1; i < lista.size(); i++){
                    trajPt2.add(lista.get(i));
                }
                
                session.setAttribute("listaTrajPt2", trajPt2);
                requestDispatcher = request.getRequestDispatcher("/view/corrida/encontro.jsp");
                requestDispatcher.forward(request, response);
                break;
            }
            case "/corrida/encontroJS": {
                int meetPoint = env.getFinalIndex();
                HttpSession session = request.getSession();
                List<Ponto> lista = (ArrayList<Ponto>) session.getAttribute("listaPtsTraj");
                List<Ponto> trajPt2 = new ArrayList<>();
                
                for(int i = meetPoint; i < lista.size(); i++){
                    trajPt2.add(lista.get(i));
                }
                
                GsonBuilder builder = new GsonBuilder();
                builder.setDateFormat("dd-MM-yyyy hh:mm:ss.S");
                Gson gson = builder.create();

                response.setContentType("application/json");
                response.setCharacterEncoding("utf-8");
                PrintWriter out = response.getWriter();

                out.print(gson.toJson(trajPt2));
                break;
            }
            case "/corrida/escolheuDestino": {
                int id = Integer.parseInt(request.getParameter("id"));
                HttpSession session = request.getSession();
                ReqCorrida rc = (ReqCorrida) session.getAttribute("reqAtual");
                
                Corrida corr = new Corrida();
                corr.setId_req_org(rc.getId());
                corr.setId_pf(id);
                corr.setEncerrada(false);
                
                corr.setData(Constantes.getTodaysDate());
                
                try {
                    daoFactory = new DAOFactory();
                    CorridaDAO dao = daoFactory.getCorridaDAO();
                    
                    dao.create(corr);
                    
                    daoFactory.close();
                } catch (ClassNotFoundException | SQLException ex) {
                    Logger.getLogger(CorridaController.class.getName()).log(Level.SEVERE, null, ex);
                }
                
                session.setAttribute("comecouPt2", false);
                session.setAttribute("aguardandoTax", true);
                session.setAttribute("corrAtual", corr);
                requestDispatcher = request.getRequestDispatcher("/view/corrida/espera.jsp");
                requestDispatcher.forward(request, response);
                break;
            }
            case "/corrida/aguardandoTax": {
                HttpSession session = request.getSession();
                Corrida corrAtual = (Corrida) session.getAttribute("corrAtual");
                int id = corrAtual.getId();
                
                try {
                    daoFactory = new DAOFactory();
                    CorridaDAO dao = daoFactory.getCorridaDAO();
                    
                    corrAtual = dao.readHalf(id);
                    
                    daoFactory.close();
                } catch (ClassNotFoundException | SQLException ex) {
                    Logger.getLogger(CorridaController.class.getName()).log(Level.SEVERE, null, ex);
                }
                session.setAttribute("corrAtual", corrAtual);
                
                if(corrAtual.isEmAndamento()){
                    session.setAttribute("aguardandoTax", false);
                    session.setAttribute("readyForPart2", true);
                }
                requestDispatcher = request.getRequestDispatcher("/view/corrida/espera.jsp");
                requestDispatcher.forward(request, response);
                break;
            }
            case "/corrida/comecouCorridapt2": {
                int id = Integer.parseInt(request.getParameter("id"));
                Corrida corr = new Corrida();
                corr.setId(id);
                corr.setEmAndamento(true);
                HttpSession session = request.getSession();
                session.setAttribute("corrAtualPTaxi", corr);
                
                try {
                    daoFactory = new DAOFactory();
                    CorridaDAO dao = daoFactory.getCorridaDAO();
                    
                    dao.updateEmAndById(corr);
                    
                    corr = dao.readHalf(id);
                    
                    daoFactory.close();
                } catch (ClassNotFoundException | SQLException ex) {
                    Logger.getLogger(CorridaController.class.getName()).log(Level.SEVERE, null, ex);
                }
                ReqCorrida rc = null;
                try {
                    daoFactory = new DAOFactory();
                    ReqCorridaDAO dao = daoFactory.getReqCorridaDAO();
                    
                    rc = dao.read(corr.getId_req_org());
                    
                    daoFactory.close();
                } catch (ClassNotFoundException | SQLException ex) {
                    Logger.getLogger(CorridaController.class.getName()).log(Level.SEVERE, null, ex);
                }
                
                try {
                    daoFactory = new DAOFactory();
                    PontoDAO dao = daoFactory.getPontoDAO();
                    
                    listaPtsTrajPt2 = dao.allPointsById_traj2(rc.getTraj_esc(), rc.getId_pi());
                    
                    finalPoint = dao.read(corr.getId_pf());
                    
                    daoFactory.close();
                } catch (ClassNotFoundException | SQLException ex) {
                    Logger.getLogger(CorridaController.class.getName()).log(Level.SEVERE, null, ex);
                }
                
                env2 = new Enviador(listaPtsTrajPt2, rc.getId_pi());
                Thread t = new Thread(env2);
                t.start();
                
                
                
                requestDispatcher = request.getRequestDispatcher("/view/corrida/espera.jsp");
                requestDispatcher.forward(request, response);
                break;
            }
            case "/corrida/visualCorrPt2": {
                request.setAttribute("pontoAtualDesloc", listaPtsTrajPt2.get(0));
                requestDispatcher = request.getRequestDispatcher("/view/corrida/corridaPt2.jsp");
                requestDispatcher.forward(request, response);
                break;
            }
            case "/corrida/fim": {
                HttpSession session = request.getSession();
                List<Ponto> listaPontos = (ArrayList<Ponto>) session.getAttribute("listaPtsTraj");
                Corrida corrAtual = (Corrida) session.getAttribute("corrAtual");
                ReqCorrida reqCorrAtual = (ReqCorrida) session.getAttribute("reqAtual");
                
                int pontoInicialId = reqCorrAtual.getId_pi();
                int pontoFinalId = corrAtual.getId_pf();
                List<Ponto> listaPtsClFez = new ArrayList<>();
                
                for(int i=0; i<= (pontoFinalId-pontoInicialId); i++){
                    for (Ponto pt : listaPontos) {
                        if(pt.getId()==(pontoInicialId+i)){
                            listaPtsClFez.add(pt);
                        }
                    }
                }
                
                double totalDistance = 0;
                
                for(int i=0; i < (pontoFinalId-pontoInicialId); i++){
                    Ponto p1 = listaPtsClFez.get(i);
                    Ponto p2 = listaPtsClFez.get(i+1);
                    totalDistance += CorridaController.distFrom(p1.getLat(), p1.getLng(), p2.getLat(), p2.getLng());
                }
                TaxistaDAO dao;
                Taxista taxista = null;
                try {
                    daoFactory = new DAOFactory();
                    dao = daoFactory.getTaxistaDAO();

                    taxista = dao.read(reqCorrAtual.getId_tax());

                    daoFactory.close();
                } catch (ClassNotFoundException | SQLException ex) {
                    Logger.getLogger(UsuarioController.class.getName()).log(Level.SEVERE, null, ex);
                }
                
                BigDecimal valor_cpkr = taxista.getValor_cpkr();
                double valor_cpkr2 = valor_cpkr.doubleValue();
                double custoCorrida = valor_cpkr2 * totalDistance;
                int id_corrida = corrAtual.getId();
                
                CorridaDAO dao2;
                try {
                    daoFactory = new DAOFactory();
                    dao2 = daoFactory.getCorridaDAO();

                    dao2.updateDistanciaById(totalDistance, id_corrida);
                    dao2.updateValorById(custoCorrida, id_corrida);
                    dao2.encerrarCorridaById(id_corrida);

                    daoFactory.close();
                } catch (ClassNotFoundException | SQLException ex) {
                    Logger.getLogger(UsuarioController.class.getName()).log(Level.SEVERE, null, ex);
                }
                
                Corrida corrAtualizada = null;
                
                try {
                    daoFactory = new DAOFactory();
                    dao2 = daoFactory.getCorridaDAO();

                    corrAtualizada = dao2.read(id_corrida);

                    daoFactory.close();
                } catch (ClassNotFoundException | SQLException ex) {
                    Logger.getLogger(UsuarioController.class.getName()).log(Level.SEVERE, null, ex);
                }
                
                session.setAttribute("corridaAt", corrAtualizada);
                session.setAttribute("taxista", taxista);
                session.setAttribute("readyForPart2", false);
                
                requestDispatcher = request.getRequestDispatcher("/view/corrida/fim.jsp");
                requestDispatcher.forward(request, response);
                break;
            }
            case "/corrida/taxistaAguFim": {
                Corrida corrAtualizada = null;
                CorridaDAO dao2;
                HttpSession session = request.getSession();
                Corrida corrDesat = (Corrida) session.getAttribute("corrAtualPTaxi");
                int id_corrida = corrDesat.getId();
                
                try {
                    daoFactory = new DAOFactory();
                    dao2 = daoFactory.getCorridaDAO();

                    corrAtualizada = dao2.read(id_corrida);

                    daoFactory.close();
                } catch (ClassNotFoundException | SQLException ex) {
                    Logger.getLogger(UsuarioController.class.getName()).log(Level.SEVERE, null, ex);
                }
                
                session.setAttribute("corrAtTaxSide", corrAtualizada);
                
                if(corrAtualizada.isEncerrada()){
                    requestDispatcher = request.getRequestDispatcher("/view/corrida/fim.jsp");
                    requestDispatcher.forward(request, response);
                    break;
                } else {
                    requestDispatcher = request.getRequestDispatcher("/view/corrida/espera.jsp");
                    requestDispatcher.forward(request, response);
                    break;
                }
            }
            case "/corrida/atualizaDesloc": {
                HttpSession session = request.getSession();
                ReqCorrida rc = (ReqCorrida) session.getAttribute("reqAtual");
                Ponto pontoAtualTaxi = env.atualizaCliente();
                
                    GsonBuilder builder = new GsonBuilder();
                    builder.setDateFormat("dd-MM-yyyy hh:mm:ss.S");
                    Gson gson = builder.create();

                    response.setContentType("application/json");
                    response.setCharacterEncoding("utf-8");
                    PrintWriter out = response.getWriter();

                    out.print(gson.toJson(pontoAtualTaxi));
                    break;
                
            }
            case "/corrida/atualizaDesloc2": {
                HttpSession session = request.getSession();
                ReqCorrida rc = (ReqCorrida) session.getAttribute("reqAtual");
                Ponto pontoAtualTaxi = env2.atualizaCliente();
                
                    GsonBuilder builder = new GsonBuilder();
                    builder.setDateFormat("dd-MM-yyyy hh:mm:ss.S");
                    Gson gson = builder.create();

                    response.setContentType("application/json");
                    response.setCharacterEncoding("utf-8");
                    PrintWriter out = response.getWriter();

                    out.print(gson.toJson(pontoAtualTaxi));
                    break;
                
            }
        }
    }

    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        RequestDispatcher requestDispatcher;
        
        switch (request.getServletPath()) {
            case "/corrida/atualizar": {
                HttpSession session = request.getSession();
                ReqCorrida rc = (ReqCorrida) session.getAttribute("reqAtual");
                Ponto pontoAtualTaxi = env.atualizaCliente();
                
                DAOFactory daoFactory;
                PontoDAO dao;
                Ponto pto = null;
                try {
                    daoFactory = new DAOFactory();
                    dao = daoFactory.getPontoDAO();

                    pto = dao.read(rc.getId_pi());

                    daoFactory.close();
                } catch (ClassNotFoundException | IOException | SQLException ex) {
                    Logger.getLogger(Enviador.class.getName()).log(Level.SEVERE, null, ex);
                }
                
                if(pontoAtualTaxi.getLat() == pto.getLat() && pontoAtualTaxi.getLng() == pto.getLng()){
                    response.sendRedirect(request.getContextPath() + "/corrida/encontro");
                    break;
                } else {
                    request.setAttribute("pontoAtualDesloc", pontoAtualTaxi);
                    requestDispatcher = request.getRequestDispatcher("/view/corrida/index.jsp");
                    requestDispatcher.forward(request, response);
                    break;
                }
            }
            case "/corrida/atualizar2": {
                Ponto pontoAtualTaxi = env2.atualizaCliente();
                
                if(pontoAtualTaxi.getLat() == finalPoint.getLat() && pontoAtualTaxi.getLng() == finalPoint.getLng()){
                    response.sendRedirect(request.getContextPath() + "/corrida/fim");
                    break;
                } else {
                    request.setAttribute("pontoAtualDesloc", pontoAtualTaxi);
                    requestDispatcher = request.getRequestDispatcher("/view/corrida/corridaPt2.jsp");
                    requestDispatcher.forward(request, response);
                    break;
                }
            }
            case "/corrida/notaCliente": {
                int nota = Integer.parseInt(request.getParameter("notaSatsCl"));
                HttpSession session = request.getSession();
                Corrida corrida = (Corrida) session.getAttribute("corridaAt");
                
                CorridaDAO dao2;
                DAOFactory daoFactory;
                try {
                    daoFactory = new DAOFactory();
                    dao2 = daoFactory.getCorridaDAO();

                    dao2.atribuirNotaAoTaxista(nota, corrida.getId());

                    daoFactory.close();
                } catch (ClassNotFoundException | SQLException ex) {
                    Logger.getLogger(UsuarioController.class.getName()).log(Level.SEVERE, null, ex);
                }
                
                requestDispatcher = request.getRequestDispatcher("/view/usuario/index.jsp");
                requestDispatcher.forward(request, response);
                break;
            }
            case "/corrida/notaTaxista": {
                int nota = Integer.parseInt(request.getParameter("notaSatsTax"));
                HttpSession session = request.getSession();
                Corrida corrida = (Corrida) session.getAttribute("corrAtTaxSide");
                
                CorridaDAO dao2;
                DAOFactory daoFactory;
                try {
                    daoFactory = new DAOFactory();
                    dao2 = daoFactory.getCorridaDAO();

                    dao2.atribuirNotaAoCliente(nota, corrida.getId());

                    daoFactory.close();
                } catch (ClassNotFoundException | SQLException ex) {
                    Logger.getLogger(UsuarioController.class.getName()).log(Level.SEVERE, null, ex);
                }
                
                requestDispatcher = request.getRequestDispatcher("/view/usuario/index.jsp");
                requestDispatcher.forward(request, response);
                break;
            }
        }
    }
    
    @Override
    public String getServletInfo() {
        return "Short description";
    }

}
