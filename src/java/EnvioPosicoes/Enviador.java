package EnvioPosicoes;

import dao.DAOFactory;
import dao.PontoDAO;
import java.io.IOException;
import static java.lang.Thread.sleep;
import java.sql.SQLException;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;
import model.Ponto;

public class Enviador implements Runnable {

    private final List<Ponto> lista;
    private int index;
    private Ponto ptoEncontro;
    private final long[] vetorInt;
    
    public Enviador(List<Ponto> lista, int id_ptoEncontro){
        this.lista = lista;
        this.index = 0;
        
        DAOFactory daoFactory;
        PontoDAO dao;
        try {
            daoFactory = new DAOFactory();
            dao = daoFactory.getPontoDAO();

            this.ptoEncontro = dao.read(id_ptoEncontro);

            daoFactory.close();
        } catch (ClassNotFoundException | IOException | SQLException ex) {
            Logger.getLogger(Enviador.class.getName()).log(Level.SEVERE, null, ex);
        }
                
        vetorInt = new long[lista.size() - 1];
        for (int i = 1; i <= vetorInt.length; i++) {
            Ponto p = lista.get(i-1);
            Ponto p2 = lista.get(i);
            long diff = p2.getPt().getTime() - p.getPt().getTime();
            vetorInt[i-1] = diff;
        }
    }
    
    public Ponto atualizaCliente(){
        if(index < lista.size())
            return lista.get(index);
        else
            return lista.get(lista.size() - 1);
    }
    
    public int getFinalIndex(){
        return this.index;
    }
    
    @Override
    public void run() {
        for (Ponto el : lista) {
            try {
                sleep(5000);
                //sleep(this.vetorInt[this.index]);
            } catch (InterruptedException ex) {
                Logger.getLogger(Enviador.class.getName()).log(Level.SEVERE, null, ex);
            }
            if((index + 1) < lista.size()){
                Ponto p = lista.get(index + 1);
                if(p.getLat() == ptoEncontro.getLat() && p.getLng() == ptoEncontro.getLng()){
                    this.index++;
                    break;
                } else {
                    this.index++;
                }
            } else {
                Ponto p = lista.get(index);
                if(p.getLat() == ptoEncontro.getLat() && p.getLng() == ptoEncontro.getLng()){
                    this.index++;
                    break;
                } else {
                    this.index++;
                }
            }
            
        }
    }

}
