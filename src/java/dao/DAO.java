package dao;

import java.sql.Connection;
import java.util.List;

public abstract class DAO<T> {
    
    protected Connection connection;
    
    public DAO(Connection connection) {
        this.connection = connection;
    }
    
    public abstract void create(T obj);
    
    public abstract T read(int id);
    
    public abstract void update(T obj);
    
    public abstract void delete(int id);
    
    public abstract List<T> all();
    
}
