package dao;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;
import model.Taxista;
import model.ViewReqsTaxista;

public class TaxistaDAO extends DAO<Taxista> {
    private final String createQuery = "INSERT INTO taxista(id_t, valor_cpkr) "
            + "VALUES (?, ?);";
    
    private final String readQuery = "SELECT * FROM usuario u, taxista t WHERE u.id = t.id_t AND u.id = ?;";
    
    private final String updateQuery = "UPDATE usuario "
                                     + "SET login = ?, senha = md5(?), pnome = ?, snome = ?, dtNasc = ?,"
                                     + " tel = ?, sexo = ?, ativo = ?, cpf = ? "
                                     + "WHERE id = ?;";
    
    private final String updateQuery2 = "UPDATE taxista "
                                     + "SET valor_cpkr = ? "
                                     + "WHERE id_t = ?;";
    
    private final String deleteQuery2  = "DELETE FROM taxista WHERE id_t = ?;";
    private final String deleteQuery  = "DELETE FROM usuario WHERE id = ?;";
    
    private final String allQuery = "SELECT u.id, u.nome FROM usuario u, taxista t WHERE u.id=t.id_t;";
    
    private final String verReqCorridasByTaxIdQuery = "SELECT u.pnome, u.snome, u.tel, u.sexo, u.dtnasc, u.id AS user_id, \n" +
"                                        r.id AS req_id, r.id_clt, r.id_tax, p.lat, p.lng FROM reqCorrida r, usuario u, pontos p\n" +
"                                        WHERE r.id_clt = u.id AND r.id_tax = ? AND r.aceita IS NULL AND p.id = r.id_pi;";
    
    private final String verCorridasByTaxIdQuery = "SELECT u.pnome, u.snome, u.tel, u.sexo, u.dtnasc, u.id AS user_id,\n" +
"                                        r.id AS req_id, r.id_clt, r.id_tax, p.lat, p.lng, c.id AS id_corr FROM reqCorrida r, usuario u, corrida c, pontos p\n" +
"                                        WHERE r.id_clt = u.id AND r.id_tax = ? AND r.aceita = true AND r.id = c.id_req_org AND c.id_pf = p.id AND c.encerrada = false;";
    
    private final String aceitarOuRecusarQuery = "UPDATE reqCorrida SET aceita = ? WHERE id = ?;";
    private final String aceitarOuRecusarQuery2 = "UPDATE reqCorrida SET analisada = true WHERE id = ?;";
    
    public TaxistaDAO(Connection connection) {
        super(connection);
    }
    
    @Override
    public void create(Taxista taxista) {
        try {
            PreparedStatement statement = connection.prepareStatement(createQuery);
            
            statement.setInt(1, taxista.getId());
            statement.setBigDecimal(2, taxista.getValor_cpkr());

            statement.executeQuery();
        } catch (SQLException ex) {
            Logger.getLogger(TaxistaDAO.class.getName()).log(Level.SEVERE, null, ex);
        }
    }

    @Override
    public Taxista read(int id) {
        Taxista taxista = null;
        try {
            PreparedStatement statement = connection.prepareStatement(readQuery);
            
            statement.setInt(1, id);
            
            ResultSet result = statement.executeQuery();            
            
            if (result.next()) {
                taxista = new Taxista();
                taxista.setId(result.getInt("id"));
                taxista.setLogin(result.getString("login"));
                taxista.setSenha(result.getString("senha"));
                taxista.setPnome(result.getString("pnome"));
                taxista.setSnome(result.getString("snome"));
                taxista.setTel(result.getString("tel"));
                taxista.setSexo(result.getString("sexo"));
                taxista.setAtivo(result.getBoolean("ativo"));
                taxista.setCpf(result.getString("cpf"));
                taxista.setDtNasc(result.getDate("dtNasc"));
                taxista.setValor_cpkr(result.getBigDecimal("valor_cpkr"));
            }
            
            return taxista;
        } catch (SQLException ex) {
            Logger.getLogger(TaxistaDAO.class.getName()).log(Level.SEVERE, null, ex);
        }
        
        return null;
    }

    @Override
    public void update(Taxista taxista) {
        try {
            PreparedStatement statement = connection.prepareStatement(updateQuery);
            
            statement.setString(1, taxista.getLogin());
            statement.setString(2, taxista.getSenha());
            statement.setString(3, taxista.getPnome());
            statement.setString(4, taxista.getSnome());
            statement.setDate(5, taxista.getDtNasc());
            statement.setString(6, taxista.getTel());
            statement.setString(7, taxista.getSexo());
            statement.setBoolean(8, taxista.isAtivo());
            statement.setString(9, taxista.getCpf());
            
            statement.setInt(10, taxista.getId());
            
            statement.executeUpdate();
            
            PreparedStatement statement2 = connection.prepareStatement(updateQuery2);
            
            statement2.setBigDecimal(1, taxista.getValor_cpkr());
            
            statement2.setInt(2, taxista.getId());
            
            statement2.executeUpdate();
            
        } catch (SQLException ex) {
            Logger.getLogger(TaxistaDAO.class.getName()).log(Level.SEVERE, null, ex);
        }
    }
    
    public void update2(Taxista taxista) {
        try {
            PreparedStatement statement = connection.prepareStatement(updateQuery2);
            
            statement.setBigDecimal(1, taxista.getValor_cpkr());
            statement.setInt(2, taxista.getId());
            
            statement.executeUpdate();
        } catch (SQLException ex) {
            Logger.getLogger(TaxistaDAO.class.getName()).log(Level.SEVERE, null, ex);
        }
    }

    @Override
    public void delete(int id) {
        try {
            PreparedStatement statement2 = connection.prepareStatement(deleteQuery2);
            statement2.setInt(1, id);
            statement2.executeUpdate();
            PreparedStatement statement = connection.prepareStatement(deleteQuery);
            statement.setInt(1, id);
            statement.executeUpdate();
        } catch (SQLException ex) {
            Logger.getLogger(TaxistaDAO.class.getName()).log(Level.SEVERE, null, ex);
        }
    }

    @Override
    public List<Taxista> all() {
        try {
            PreparedStatement statement = connection.prepareStatement(allQuery);
            
            ResultSet result = statement.executeQuery();
            
            List<Taxista> taxistas = new ArrayList<>();
            
            Taxista taxista;
            
            while (result.next()) {
                taxista = new Taxista();
                
                taxista.setId(result.getInt("id"));
                taxista.setPnome(result.getString("pnome"));
                
                taxistas.add(taxista);
            }
            
            return taxistas;
        } catch (SQLException ex) {
            Logger.getLogger(TaxistaDAO.class.getName()).log(Level.SEVERE, null, ex);
        }
        
        return null;
    }
    
    public List<ViewReqsTaxista> verReqCorridasByTaxId(int id){
        List<ViewReqsTaxista> lista = new ArrayList<>();
        try {
            PreparedStatement statement = connection.prepareStatement(verReqCorridasByTaxIdQuery);
            
            statement.setInt(1, id);
            
            ResultSet result = statement.executeQuery();      
            ViewReqsTaxista v;
            
            while (result.next()) {
                v = new ViewReqsTaxista();
                v.setPnome_clt(result.getString("pnome"));
                v.setSnome_clt(result.getString("snome"));
                v.setTel_clt(result.getString("tel"));
                v.setSexo_clt(result.getString("sexo"));
                v.setDtNasc_clt(result.getDate("dtnasc"));
                v.setId_clt(result.getInt("id_clt"));
                v.setId_req(result.getInt("req_id"));
                v.setId_tax(result.getInt("id_tax"));
                v.setLat_req(result.getDouble("lat"));
                v.setLng_req(result.getDouble("lng"));
                lista.add(v);
            }
            
            return lista;
        } catch (SQLException ex) {
            Logger.getLogger(TaxistaDAO.class.getName()).log(Level.SEVERE, null, ex);
        }
        
        return null;
    }
    
    public List<ViewReqsTaxista> verCorridasByTaxId(int id){
        List<ViewReqsTaxista> lista = new ArrayList<>();
        try {
            PreparedStatement statement = connection.prepareStatement(verCorridasByTaxIdQuery);
            
            statement.setInt(1, id);
            
            ResultSet result = statement.executeQuery();      
            ViewReqsTaxista v;
            
            while (result.next()) {
                v = new ViewReqsTaxista();
                v.setPnome_clt(result.getString("pnome"));
                v.setSnome_clt(result.getString("snome"));
                v.setTel_clt(result.getString("tel"));
                v.setSexo_clt(result.getString("sexo"));
                v.setDtNasc_clt(result.getDate("dtnasc"));
                v.setId_clt(result.getInt("id_clt"));
                v.setId_req(result.getInt("req_id"));
                v.setId_tax(result.getInt("id_tax"));
                v.setLat_dest(result.getDouble("lat"));
                v.setLng_dest(result.getDouble("lng"));
                v.setId_corr(result.getInt("id_corr"));
                lista.add(v);
            }
            
            return lista;
        } catch (SQLException ex) {
            Logger.getLogger(TaxistaDAO.class.getName()).log(Level.SEVERE, null, ex);
        }
        
        return null;
    }
    
    public void aceitarOuRecusarCorrida(boolean op, int id){
        try {
            PreparedStatement statement = connection.prepareStatement(aceitarOuRecusarQuery);
            
            statement.setBoolean(1, op);
            statement.setInt(2, id);
            
            statement.executeUpdate();
        } catch (SQLException ex) {
            Logger.getLogger(TaxistaDAO.class.getName()).log(Level.SEVERE, null, ex);
        }
        try {
            PreparedStatement statement2 = connection.prepareStatement(aceitarOuRecusarQuery2);
            
            statement2.setInt(1, id);
            
            statement2.executeUpdate();
        } catch (SQLException ex) {
            Logger.getLogger(TaxistaDAO.class.getName()).log(Level.SEVERE, null, ex);
        }
    }
}
