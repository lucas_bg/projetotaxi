package dao;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;
import model.Ponto;

public class PontoDAO extends DAO<Ponto> {
    
    private final String allPointsById_trajQuery = "SELECT * FROM pontos WHERE id_traj = ? ORDER BY pt;";
    
    private final String allPointsById_trajQuery2 = "SELECT * FROM pontos WHERE id_traj = ? AND id >= ? ORDER BY pt;";

    private final String pontoByIdQuery = "SELECT * FROM pontos WHERE id = ?;";
    
    private final String searchNextQuery = "SELECT DISTINCT * FROM pontos WHERE lat >= ?-0.003 AND lat <= ?+0.003 \n" +
"	AND lng >= ?-0.003 AND lng <= ?+0.003;";
    
    private final String headByFoundIdQuery = "SELECT * FROM (\n" +
"	SELECT p.id, p.id_traj, p.id_tax, t.firstt, p.lat, p.lng FROM (\n" +
"		SELECT id_traj, MIN(pt) AS firstt FROM pontos GROUP BY id_traj\n" +
"	) t \n" +
"	JOIN pontos p ON p.id_traj = t.id_traj AND t.firstt=p.pt) a \n" +
"WHERE a.id_traj = ?;";
    
    public PontoDAO(Connection connection) {
        super(connection);
    }
    
    public List<Ponto> allPointsById_traj(int id_traj){
        List<Ponto> lista = new ArrayList<>();
        try {
            PreparedStatement statement = connection.prepareStatement(allPointsById_trajQuery);
            
            statement.setInt(1, id_traj);
            
            ResultSet result  = statement.executeQuery();
            
            Ponto pto;
            
            while (result.next()) {
                pto = new Ponto();
                
                pto.setId(result.getInt("id"));
                pto.setId_tax(result.getInt("id_tax"));
                pto.setId_traj(result.getInt("id_traj"));
                pto.setLat(result.getDouble("lat"));
                pto.setLng(result.getDouble("lng"));
                pto.setPt(result.getTimestamp("pt"));
                
                lista.add(pto);
            }
            
            return lista;
        } catch (SQLException ex) {
            Logger.getLogger(UsuarioDAO.class.getName()).log(Level.SEVERE, null, ex);
        }
         
        return null;
    }
    
    public List<Ponto> allPointsById_traj2(int id_traj, int id_pi){
        List<Ponto> lista = new ArrayList<>();
        try {
            PreparedStatement statement = connection.prepareStatement(allPointsById_trajQuery2);
            
            statement.setInt(1, id_traj);
            statement.setInt(2, id_pi);
            
            ResultSet result  = statement.executeQuery();
            
            Ponto pto;
            
            while (result.next()) {
                pto = new Ponto();
                
                pto.setId(result.getInt("id"));
                pto.setId_tax(result.getInt("id_tax"));
                pto.setId_traj(result.getInt("id_traj"));
                pto.setLat(result.getDouble("lat"));
                pto.setLng(result.getDouble("lng"));
                pto.setPt(result.getTimestamp("pt"));
                
                lista.add(pto);
            }
            
            return lista;
        } catch (SQLException ex) {
            Logger.getLogger(UsuarioDAO.class.getName()).log(Level.SEVERE, null, ex);
        }
         
        return null;
    }
    
    public List<Ponto> searchNextQuery(double lat, double lng){
        try {
            PreparedStatement statement2 = connection.prepareStatement(searchNextQuery);
            
            statement2.setDouble(1, lat);
            statement2.setDouble(2, lat);
            statement2.setDouble(3, lng);
            statement2.setDouble(4, lng);
            
            ResultSet result2  = statement2.executeQuery();
            
            List<Ponto> lista = new ArrayList<>();
            Ponto pto;
            
            while (result2.next()) {
                pto = new Ponto();
                
                pto.setId(result2.getInt("id"));
                pto.setId_tax(result2.getInt("id_tax"));
                pto.setId_traj(result2.getInt("id_traj"));
                pto.setLat(result2.getDouble("lat"));
                pto.setLng(result2.getDouble("lng"));
                pto.setPt(result2.getTimestamp("pt"));
                
                lista.add(pto);
            }
            
            return lista;
        } catch (SQLException ex) {
            Logger.getLogger(UsuarioDAO.class.getName()).log(Level.SEVERE, null, ex);
        }
         
        return null;
    }
    
    public Ponto headByFoundId(int id){
        try {
            PreparedStatement statement = connection.prepareStatement(headByFoundIdQuery);
            
            statement.setInt(1, id);
            
            ResultSet result  = statement.executeQuery();
            Ponto pto = new Ponto();
            
            if (result.next()) {
                pto.setId(id);
                pto.setId_tax(result.getInt("id_tax"));
                pto.setId_traj(result.getInt("id_traj"));
                pto.setLat(result.getDouble("lat"));
                pto.setLng(result.getDouble("lng"));
                pto.setPt(result.getTimestamp("firstt"));
            }
            return pto;

        } catch (SQLException ex) {
            Logger.getLogger(UsuarioDAO.class.getName()).log(Level.SEVERE, null, ex);
        }
        return null;
    }

    @Override
    public void create(Ponto obj) {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public Ponto read(int id) {
        try {
            PreparedStatement statement = connection.prepareStatement(pontoByIdQuery);
            
            statement.setInt(1, id);
            
            ResultSet result  = statement.executeQuery();
            Ponto pto = new Ponto();
            
            if (result.next()) {
                pto.setId(id);
                pto.setId_tax(result.getInt("id_tax"));
                pto.setId_traj(result.getInt("id_traj"));
                pto.setLat(result.getDouble("lat"));
                pto.setLng(result.getDouble("lng"));
                pto.setPt(result.getTimestamp("pt"));
            }
            return pto;

        } catch (SQLException ex) {
            Logger.getLogger(UsuarioDAO.class.getName()).log(Level.SEVERE, null, ex);
        }
        return null;
    }

    @Override
    public void update(Ponto obj) {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public void delete(int id) {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public List<Ponto> all() {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

}
