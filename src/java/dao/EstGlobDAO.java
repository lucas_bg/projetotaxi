package dao;

import java.sql.Connection;
import java.sql.Date;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;
import model.EstGlob;
import model.Query1_1;
import model.Query2_1;
import model.Query2_2;
import model.Query3_1;
import model.Query3_2;
import model.Query3_3;
import model.Query3_4;
import model.Query3_5;
import model.Query3_6;
import model.Query4_1;

public class EstGlobDAO extends DAO<EstGlob> {

    private final String query1_1 = "SELECT \n" +
    "COUNT(*) as total_corr, \n" +
    "avg(distancia) as dist_media, \n" +
    "min(distancia) as dist_min, \n" +
    "max(distancia) as dist_max, \n" +
    "avg(valor) as valor_media, \n" +
    "min(valor) as valor_min, \n" +
    "max(valor) as valor_max, \n" +
    "avg(valor/distancia) as valor_cpkr_media, \n" +
    "min(valor/distancia) as valor_cpkr_min, \n" +
    "max(valor/distancia) as valor_cpkr_max\n" +
    "FROM corrida\n" +
    "WHERE dataa BETWEEN ? AND ?;";
    
    private final String query2_1 = "SELECT \n" +
    "COUNT(*) as qtdade_total_corr_marca, \n" +
    "avg(valor/distancia) as valor_cpkr_media_marca, \n" +
    "min(valor/distancia) as valor_cpkr_min_marca, \n" +
    "max(valor/distancia) as valor_cpkr_max_marca \n" +
    "FROM corrida c, taxista t, usuario t2, reqCorrida r, veiculo v\n" +
    "WHERE \n" +
    "t.id_t = t2.id\n" +
    "AND c.id_req_org = r.id\n" +
    "AND v.id_usuario = t.id_t\n" +
    "AND c.dataa BETWEEN ? AND ?\n" +
    "GROUP BY v.marca;";
    
    private final String query2_2 = "SELECT \n" +
    "COUNT(*) as qtdade_total_corr_modelo, \n" +
    "avg(valor/distancia) as valor_cpkr_media_modelo, \n" +
    "min(valor/distancia) as valor_cpkr_min_modelo, \n" +
    "max(valor/distancia) as valor_cpkr_max_modelo \n" +
    "FROM corrida c, taxista t, usuario t2, reqCorrida r, veiculo v\n" +
    "WHERE \n" +
    "t.id_t = t2.id\n" +
    "AND c.id_req_org = r.id\n" +
    "AND v.id_usuario = t.id_t\n" +
    "AND c.dataa BETWEEN ? AND ?\n" +
    "GROUP BY v.modelo;";
    
    private final String query3_1 = "SELECT u.pnome, COUNT(*) as num FROM usuario u, corrida c, reqCorrida r\n" +
    "WHERE\n" +
    "r.id = c.id_req_org\n" +
    "AND r.id_clt = u.id\n" +
    "AND c.dataa BETWEEN ? AND ?\n" +
    "GROUP BY u.id\n" +
    "ORDER BY num DESC\n" +
    "LIMIT 10;";
    
    private final String query3_2 = "SELECT u.pnome, COUNT(*) as num FROM taxista t, corrida c, reqCorrida r, usuario u\n" +
    "WHERE\n" +
    "r.id = c.id_req_org\n" +
    "AND r.id_tax = t.id_t\n" +
    "AND u.id = t.id_t\n" +
    "AND c.dataa BETWEEN ? AND ?\n" +
    "GROUP BY u.id\n" +
    "ORDER BY num DESC\n" +
    "LIMIT 10;";
    
    private final String query3_3 = "SELECT u.pnome, sum(c.valor) as num FROM usuario u, corrida c, reqCorrida r\n" +
    "WHERE\n" +
    "r.id = c.id_req_org\n" +
    "AND r.id_clt = u.id\n" +
    "AND c.dataa BETWEEN ? AND ?\n" +
    "GROUP BY u.id\n" +
    "ORDER BY num DESC\n" +
    "LIMIT 10;";
    
    private final String query3_4 = "SELECT u.pnome, sum(c.valor) as num FROM taxista t, corrida c, reqCorrida r, usuario u\n" +
    "WHERE\n" +
    "r.id = c.id_req_org\n" +
    "AND r.id_tax = t.id_t\n" +
    "AND u.id = t.id_t\n" +
    "AND c.dataa BETWEEN ? AND ?\n" +
    "GROUP BY u.id\n" +
    "ORDER BY num DESC\n" +
    "LIMIT 10;";
    
    private final String query3_5 = "SELECT u.pnome, avg(c.nota_do_clt) as num FROM usuario u, corrida c, reqCorrida r\n" +
    "WHERE\n" +
    "r.id = c.id_req_org\n" +
    "AND r.id_clt = u.id\n" +
    "AND c.dataa BETWEEN ? AND ?\n" +
    "GROUP BY u.id\n" +
    "ORDER BY num DESC\n" +
    "LIMIT 10;";
    
    private final String query3_6 = "SELECT u.pnome, avg(c.nota_do_tax) as num FROM taxista t, corrida c, reqCorrida r, usuario u\n" +
    "WHERE\n" +
    "r.id = c.id_req_org\n" +
    "AND r.id_tax = t.id_t\n" +
    "AND u.id = t.id_t\n" +
    "AND c.dataa BETWEEN ? AND ?\n" +
    "GROUP BY u.id\n" +
    "ORDER BY num DESC\n" +
    "LIMIT 10;";
    
    private final String query4_1 = "SELECT \n" +
    "COUNT(*) AS quantidadeVezes, \n" +
    "t.login as loginTaxista, \n" +
    "u.login as loginCliente \n" +
    "FROM reqCorrida r, usuario u, usuario t, corrida c\n" +
    "WHERE \n" +
    "r.id_tax = t.id\n" +
    "AND r.id_clt = u.id\n" +
    "AND c.id_req_org = r.id\n" +
    "AND c.dataa BETWEEN ? AND ?\n" +
    "GROUP BY t.login, u.login \n" +
    "ORDER BY quantidadeVezes DESC LIMIT 10";
    
    public EstGlobDAO(Connection connection) {
        super(connection);
    }
    
    public Query1_1 funcQuery1_1(Date d1, Date d2) {
        try {
            PreparedStatement statement = connection.prepareStatement(query1_1);
            
            statement.setDate(1, d1);
            statement.setDate(2, d2);
            
            ResultSet result = statement.executeQuery();
            
            Query1_1 ret = new Query1_1();
            
            if (result.next()) {
                ret.setTotal_corr(result.getInt("total_corr"));
                ret.setDist_media(result.getDouble("dist_media"));
                ret.setDist_min(result.getDouble("dist_min"));
                ret.setDist_max(result.getDouble("dist_max"));
                ret.setValor_media(result.getDouble("valor_media"));
                ret.setValor_min(result.getDouble("valor_min"));
                ret.setValor_max(result.getDouble("valor_max"));
                ret.setValor_cpkr_media(result.getDouble("valor_cpkr_media"));
                ret.setValor_cpkr_min(result.getDouble("valor_cpkr_min"));
                ret.setValor_cpkr_max(result.getDouble("valor_cpkr_max"));
            }
            
            return ret;
        } catch (SQLException ex) {
            Logger.getLogger(UsuarioDAO.class.getName()).log(Level.SEVERE, null, ex);
        }
        return null;
    }
    
    public List<Query2_1> funcQuery2_1(Date d1, Date d2) {
        try {
            PreparedStatement statement = connection.prepareStatement(query2_1);
            
            statement.setDate(1, d1);
            statement.setDate(2, d2);
            
            ResultSet result = statement.executeQuery();
            
            List<Query2_1> lista = new ArrayList<>();
            Query2_1 el;
            
            while (result.next()) {
                el = new Query2_1();
                
                el.setQtdade_total_corr_marca(result.getInt("qtdade_total_corr_marca"));
                el.setValor_cpkr_media_marca(result.getDouble("valor_cpkr_media_marca"));
                el.setValor_cpkr_min_marca(result.getDouble("valor_cpkr_min_marca"));
                el.setValor_cpkr_max_marca(result.getDouble("valor_cpkr_max_marca"));
                
                lista.add(el);
            }
            
            return lista;
        } catch (SQLException ex) {
            Logger.getLogger(UsuarioDAO.class.getName()).log(Level.SEVERE, null, ex);
        }
        return null;
    }
    
    public List<Query2_2> funcQuery2_2(Date d1, Date d2) {
        try {
            PreparedStatement statement = connection.prepareStatement(query2_2);
            
            statement.setDate(1, d1);
            statement.setDate(2, d2);
            
            ResultSet result = statement.executeQuery();
            
            List<Query2_2> lista = new ArrayList<>();
            Query2_2 el;
            
            while (result.next()) {
                el = new Query2_2();
                
                el.setQtdade_total_corr_modelo(result.getInt("qtdade_total_corr_modelo"));
                el.setValor_cpkr_media_modelo(result.getDouble("valor_cpkr_media_modelo"));
                el.setValor_cpkr_min_modelo(result.getDouble("valor_cpkr_min_modelo"));
                el.setValor_cpkr_max_modelo(result.getDouble("valor_cpkr_max_modelo"));
                
                lista.add(el);
            }
            
            return lista;
        } catch (SQLException ex) {
            Logger.getLogger(UsuarioDAO.class.getName()).log(Level.SEVERE, null, ex);
        }
        return null;
    }
    
    public List<Query3_1> funcQuery3_1(Date d1, Date d2) {
        try {
            PreparedStatement statement = connection.prepareStatement(query3_1);
            
            statement.setDate(1, d1);
            statement.setDate(2, d2);
            
            ResultSet result = statement.executeQuery();
            
            List<Query3_1> lista = new ArrayList<>();
            Query3_1 el;
            
            while (result.next()) {
                el = new Query3_1();
                
                el.setTop_10_qtdade_corridas_cliente_nome(result.getString("pnome"));
                el.setTop_10_qtdade_corridas_cliente_qtdade(result.getInt("num"));
                
                lista.add(el);
            }
            
            return lista;
        } catch (SQLException ex) {
            Logger.getLogger(UsuarioDAO.class.getName()).log(Level.SEVERE, null, ex);
        }
        return null;
    }
    
    public List<Query3_2> funcQuery3_2(Date d1, Date d2) {
        try {
            PreparedStatement statement = connection.prepareStatement(query3_2);
            
            statement.setDate(1, d1);
            statement.setDate(2, d2);
            
            ResultSet result = statement.executeQuery();
            
            List<Query3_2> lista = new ArrayList<>();
            Query3_2 el;
            
            while (result.next()) {
                el = new Query3_2();
                
                el.setTop_10_qtdade_corridas_taxista_nome(result.getString("pnome"));
                el.setTop_10_qtdade_corridas_taxista_qtdade(result.getInt("num"));
                
                lista.add(el);
            }
            
            return lista;
        } catch (SQLException ex) {
            Logger.getLogger(UsuarioDAO.class.getName()).log(Level.SEVERE, null, ex);
        }
        return null;
    }
    
    public List<Query3_3> funcQuery3_3(Date d1, Date d2) {
        try {
            PreparedStatement statement = connection.prepareStatement(query3_3);
            
            statement.setDate(1, d1);
            statement.setDate(2, d2);
            
            ResultSet result = statement.executeQuery();
            
            List<Query3_3> lista = new ArrayList<>();
            Query3_3 el;
            
            while (result.next()) {
                el = new Query3_3();
                
                el.setTop_10_soma_valores_cliente_nome(result.getString("pnome"));
                el.setTop_10_soma_valores_cliente_soma(result.getDouble("num"));
                
                lista.add(el);
            }
            
            return lista;
        } catch (SQLException ex) {
            Logger.getLogger(UsuarioDAO.class.getName()).log(Level.SEVERE, null, ex);
        }
        return null;
    }

    public List<Query3_4> funcQuery3_4(Date d1, Date d2) {
        try {
            PreparedStatement statement = connection.prepareStatement(query3_4);
            
            statement.setDate(1, d1);
            statement.setDate(2, d2);
            
            ResultSet result = statement.executeQuery();
            
            List<Query3_4> lista = new ArrayList<>();
            Query3_4 el;
            
            while (result.next()) {
                el = new Query3_4();
                
                el.setTop_10_soma_valores_taxista_nome(result.getString("pnome"));
                el.setTop_10_soma_valores_taxista_soma(result.getDouble("num"));
                
                lista.add(el);
            }
            
            return lista;
        } catch (SQLException ex) {
            Logger.getLogger(UsuarioDAO.class.getName()).log(Level.SEVERE, null, ex);
        }
        return null;
    }
    
    public List<Query3_5> funcQuery3_5(Date d1, Date d2) {
        try {
            PreparedStatement statement = connection.prepareStatement(query3_5);
            
            statement.setDate(1, d1);
            statement.setDate(2, d2);
            
            ResultSet result = statement.executeQuery();
            
            List<Query3_5> lista = new ArrayList<>();
            Query3_5 el;
            
            while (result.next()) {
                el = new Query3_5();
                
                el.setTop_10_media_nota_cliente_nome(result.getString("pnome"));
                el.setTop_10_media_nota_cliente_media(result.getDouble("num"));
                
                lista.add(el);
            }
            
            return lista;
        } catch (SQLException ex) {
            Logger.getLogger(UsuarioDAO.class.getName()).log(Level.SEVERE, null, ex);
        }
        return null;
    }
    
    public List<Query3_6> funcQuery3_6(Date d1, Date d2) {
        try {
            PreparedStatement statement = connection.prepareStatement(query3_6);
            
            statement.setDate(1, d1);
            statement.setDate(2, d2);
            
            ResultSet result = statement.executeQuery();
            
            List<Query3_6> lista = new ArrayList<>();
            Query3_6 el;
            
            while (result.next()) {
                el = new Query3_6();
                
                el.setTop_10_media_nota_taxista_nome(result.getString("pnome"));
                el.setTop_10_media_nota_taxista_media(result.getDouble("num"));
                
                lista.add(el);
            }
            
            return lista;
        } catch (SQLException ex) {
            Logger.getLogger(UsuarioDAO.class.getName()).log(Level.SEVERE, null, ex);
        }
        return null;
    }
    
    public List<Query4_1> funcQuery4_1(Date d1, Date d2) {
        try {
            PreparedStatement statement = connection.prepareStatement(query4_1);
            
            statement.setDate(1, d1);
            statement.setDate(2, d2);
            
            ResultSet result = statement.executeQuery();
            
            List<Query4_1> lista = new ArrayList<>();
            Query4_1 el;
            
            while (result.next()) {
                el = new Query4_1();
                
                el.setTop_10_cliente_fiel_login_clt(result.getString("loginCliente"));
                el.setTop_10_cliente_fiel_login_tax(result.getString("loginTaxista"));
                el.setTop_10_cliente_fiel_qt_vezes(result.getInt("quantidadeVezes"));
                
                lista.add(el);
            }
            
            return lista;
        } catch (SQLException ex) {
            Logger.getLogger(UsuarioDAO.class.getName()).log(Level.SEVERE, null, ex);
        }
        return null;
    }
    
    @Override
    public void create(EstGlob obj) {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public EstGlob read(int id) {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public void update(EstGlob obj) {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public void delete(int id) {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public List<EstGlob> all() {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }
    
}
