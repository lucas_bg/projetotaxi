package dao;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;
import model.ReqCorrida;


public class ReqCorridaDAO extends DAO<ReqCorrida> {
    
    private final String criaReqCorr = "INSERT INTO reqCorrida(id_clt, id_tax, id_p0, id_pi, traj_esc)"
            + "VALUES (?, ?, ?, ?, ?) RETURNING id;";
    
    private final String readReqCorridaQuery = "SELECT * FROM reqCorrida WHERE id = ?;";
    
    public ReqCorridaDAO(Connection connection) {
        super(connection);
    }
    
    @Override
    public void create(ReqCorrida rc) {
        try {
            PreparedStatement statement = connection.prepareStatement(criaReqCorr);
            
            statement.setInt(1, rc.getId_clt());
            statement.setInt(2, rc.getId_tax());
            statement.setInt(3, rc.getId_p0());
            statement.setInt(4, rc.getId_pi());
            statement.setInt(5, rc.getTraj_esc());
            
            ResultSet result  = statement.executeQuery();
            
            if (result.next()) {
                rc.setId(result.getInt("id"));
            }

        } catch (SQLException ex) {
            Logger.getLogger(UsuarioDAO.class.getName()).log(Level.SEVERE, null, ex);
        }
    }

    @Override
    public ReqCorrida read(int id) {
        ReqCorrida rc = new ReqCorrida();
        try {
            PreparedStatement statement = connection.prepareStatement(readReqCorridaQuery);
            
            statement.setInt(1, id);
            
            ResultSet result  = statement.executeQuery();
            
            if (result.next()) {
                rc.setAceita(result.getBoolean("aceita"));
                rc.setAnalisada(result.getBoolean("analisada"));
                rc.setId(id);
                rc.setId_clt(result.getInt("id_clt"));
                rc.setId_tax(result.getInt("id_tax"));
                rc.setId_p0(result.getInt("id_p0"));
                rc.setId_pi(result.getInt("id_pi"));
                rc.setTraj_esc(result.getInt("traj_esc"));
                return rc;
            }
        } catch (SQLException ex) {
            Logger.getLogger(UsuarioDAO.class.getName()).log(Level.SEVERE, null, ex);
        }
        return null;
    }

    @Override
    public void update(ReqCorrida obj) {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public void delete(int id) {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public List<ReqCorrida> all() {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

}
