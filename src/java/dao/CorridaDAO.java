package dao;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;
import model.Corrida;

public class CorridaDAO extends DAO<Corrida> {

    private final String createQuery = "INSERT INTO corrida(id_req_org, id_pf, emAndamento, encerrada, dataa) "
            + "VALUES (?, ?, ?, ?, ?) RETURNING id;";
    
    private final String updateEmAndByIdQuery = "UPDATE corrida SET emAndamento = ? WHERE id = ?;";
    
    private final String updateDistanciaByIdQuery = "UPDATE corrida SET distancia = ? WHERE id = ?;";
    
    private final String updateValorByIdQuery = "UPDATE corrida SET valor = ? WHERE id = ?;";
    
    private final String encerrarCorridaByIdQuery = "UPDATE corrida SET encerrada = true WHERE id = ?;";
    
    private final String readQuery = "SELECT * FROM corrida WHERE id = ?;";
    
    private final String readHalfQuery = "SELECT * FROM corrida WHERE id = ?;";
    
    private final String atribuirNotaAoClienteQuery = "UPDATE corrida SET nota_do_clt = ? WHERE id = ?;";
    
    private final String atribuirNotaAoTaxistaQuery = "UPDATE corrida SET nota_do_tax = ? WHERE id = ?;";
    
    public CorridaDAO(Connection connection) {
        super(connection);
    }

    @Override
    public void create(Corrida corr) {
        try {
            PreparedStatement statement = connection.prepareStatement(createQuery);
            
            statement.setInt(1, corr.getId_req_org());
            statement.setInt(2, corr.getId_pf());
            statement.setBoolean(3, corr.isEmAndamento());
            statement.setBoolean(4, corr.isEncerrada());
            statement.setDate(5, corr.getData());
                        
            ResultSet result = statement.executeQuery();
            
            if (result.next()) {
                corr.setId(result.getInt("id"));
            }
        } catch (SQLException ex) {
            Logger.getLogger(UsuarioDAO.class.getName()).log(Level.SEVERE, null, ex);
        }
    }
    
    public void updateEmAndById(Corrida corr){
        try {
            PreparedStatement statement = connection.prepareStatement(updateEmAndByIdQuery);
            
            statement.setBoolean(1, corr.isEmAndamento());
            
            statement.setInt(2, corr.getId());
                        
            statement.executeUpdate();
        } catch (SQLException ex) {
            Logger.getLogger(UsuarioDAO.class.getName()).log(Level.SEVERE, null, ex);
        }
    }
    
    public void updateDistanciaById(double d, int id){
        try {
            PreparedStatement statement = connection.prepareStatement(updateDistanciaByIdQuery);
            
            statement.setDouble(1, d);
            
            statement.setInt(2, id);
                        
            statement.executeUpdate();
        } catch (SQLException ex) {
            Logger.getLogger(UsuarioDAO.class.getName()).log(Level.SEVERE, null, ex);
        }
    }
    
    public void updateValorById(double d, int id){
        try {
            PreparedStatement statement = connection.prepareStatement(updateValorByIdQuery);
            
            statement.setDouble(1, d);
            
            statement.setInt(2, id);
                        
            statement.executeUpdate();
        } catch (SQLException ex) {
            Logger.getLogger(UsuarioDAO.class.getName()).log(Level.SEVERE, null, ex);
        }
    }
    
    public void encerrarCorridaById(int id){
        try {
            PreparedStatement statement = connection.prepareStatement(encerrarCorridaByIdQuery);
            
            statement.setInt(1, id);
                        
            statement.executeUpdate();
        } catch (SQLException ex) {
            Logger.getLogger(UsuarioDAO.class.getName()).log(Level.SEVERE, null, ex);
        }
    }
    
    @Override
    public Corrida read(int id) {
        try {
            PreparedStatement statement = connection.prepareStatement(readQuery);
            
            statement.setInt(1, id);
            
            ResultSet result = statement.executeQuery();
            
            Corrida corrida = new Corrida();
            
            if (result.next()) {
                corrida.setId(result.getInt("id"));
                corrida.setId_req_org(result.getInt("id_req_org"));
                corrida.setId_pf(result.getInt("id_pf"));
                corrida.setEmAndamento(result.getBoolean("emandamento"));
                corrida.setDistancia(result.getDouble("distancia"));
                corrida.setValor(result.getDouble("valor"));
                corrida.setEncerrada(result.getBoolean("encerrada"));
            }
            
            return corrida;
        } catch (SQLException ex) {
            Logger.getLogger(UsuarioDAO.class.getName()).log(Level.SEVERE, null, ex);
        }
        
        return null;
    }
    
    //ALTERAR
    public Corrida readHalf(int id) {
        try {
            PreparedStatement statement = connection.prepareStatement(readHalfQuery);
            
            statement.setInt(1, id);
            
            ResultSet result = statement.executeQuery();
            
            Corrida corrida = new Corrida();
            
            if (result.next()) {
                corrida.setId(result.getInt("id"));
                corrida.setId_req_org(result.getInt("id_req_org"));
                corrida.setId_pf(result.getInt("id_pf"));
                corrida.setEmAndamento(result.getBoolean("emandamento"));
            }
            
            return corrida;
        } catch (SQLException ex) {
            Logger.getLogger(UsuarioDAO.class.getName()).log(Level.SEVERE, null, ex);
        }
        
        return null;
    }
    
    public void atribuirNotaAoCliente(int nota, int id){
        try {
            PreparedStatement statement = connection.prepareStatement(atribuirNotaAoClienteQuery);
            
            statement.setInt(1, nota);
            
            statement.setInt(2, id);
                        
            statement.executeUpdate();
        } catch (SQLException ex) {
            Logger.getLogger(UsuarioDAO.class.getName()).log(Level.SEVERE, null, ex);
        }
    }
    
    public void atribuirNotaAoTaxista(int nota, int id){
        try {
            PreparedStatement statement = connection.prepareStatement(atribuirNotaAoTaxistaQuery);
            
            statement.setInt(1, nota);
            
            statement.setInt(2, id);
                        
            statement.executeUpdate();
        } catch (SQLException ex) {
            Logger.getLogger(UsuarioDAO.class.getName()).log(Level.SEVERE, null, ex);
        }
    }

    @Override
    public void update(Corrida obj) {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public void delete(int id) {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public List<Corrida> all() {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

}
