package dao;

import java.sql.Connection;
import java.sql.Date;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;
import model.Historico;
import model.Historico2;

public class HistoricoDAO extends DAO<Historico> {
    
    private final String readInfoCltQuery = "SELECT \n" +
    "c.distancia,\n" +
    "c.valor,\n" +
    "c.nota_do_tax,\n" +
    "c.dataa, \n" +
    "t.pnome as tax_pname, \n" +
    "t.snome as tax_sname \n" +
    "FROM corrida c, reqCorrida r, usuario u, usuario t \n" +
    "WHERE r.id_clt = ? \n" +
    "AND r.id = c.id_req_org \n" +
    "AND r.id_clt = u.id \n" +
    "AND r.id_tax = t.id\n" +
    "AND c.dataa BETWEEN ? AND ?;";
    
    private final String readMediasCltQuery = "SELECT\n" +
    "avg(c.valor) as valor_medio,\n" +
    "avg(c.nota_do_clt) as nota_media_do_clt,\n" +
    "avg(c.distancia) as media_distancia,\n" +
    "avg(c.nota_do_tax) as nota_media_do_tax\n" +
    "FROM corrida c, reqCorrida r \n" +
    "WHERE r.id_clt = ? \n" +
    "AND r.id = c.id_req_org\n" +
    "AND c.dataa BETWEEN ? AND ?;";
    
    private final String readInfoTaxQuery = "SELECT \n" +
    "c.distancia,\n" +
    "c.valor,\n" +
    "c.nota_do_clt,\n" +
    "c.dataa, \n" +
    "u.pnome as clt_pname, \n" +
    "u.snome as clt_sname \n" +
    "FROM corrida c, reqCorrida r, usuario u, usuario t \n" +
    "WHERE r.id_tax = ? \n" +
    "AND r.id = c.id_req_org \n" +
    "AND r.id_clt = u.id \n" +
    "AND r.id_tax = t.id\n" +
    "AND c.dataa BETWEEN ? AND ?;";
    
    private final String readMediasTaxQuery = "SELECT\n" +
    "avg(c.valor) as valor_medio,\n" +
    "avg(c.nota_do_clt) as nota_media_do_clt,\n" +
    "avg(c.distancia) as media_distancia,\n" +
    "avg(c.nota_do_tax) as nota_media_do_tax\n" +
    "FROM corrida c, reqCorrida r \n" +
    "WHERE r.id_tax = ? \n" +
    "AND r.id = c.id_req_org\n" +
    "AND c.dataa BETWEEN ? AND ?;";

    public HistoricoDAO(Connection connection) {
        super(connection);
    }

    @Override
    public void create(Historico obj) {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    public List<Historico> readForCltLista(int id, Date d1, Date d2) {
        try {
            PreparedStatement statement = connection.prepareStatement(readInfoCltQuery);
            
            statement.setInt(1, id);
            statement.setDate(2, d1);
            statement.setDate(3, d2);
            
            ResultSet result = statement.executeQuery();
            
            List<Historico> lista = new ArrayList<>();
            Historico hist;
            
            while (result.next()) {
                hist = new Historico();
                
                hist.setDistancia(result.getDouble("distancia"));
                hist.setValor(result.getDouble("valor"));
                hist.setNota_do_tax(result.getInt("nota_do_tax"));
                hist.setData(result.getDate("dataa"));
                hist.setTax_pname(result.getString("tax_pname"));
                hist.setTax_sname(result.getString("tax_sname"));
                
                lista.add(hist);
            }
            
            return lista;
        } catch (SQLException ex) {
            Logger.getLogger(UsuarioDAO.class.getName()).log(Level.SEVERE, null, ex);
        }
        return null;
    }
    
    public Historico2 readForCltMedias(int id, Date d1, Date d2) {
        try {
            PreparedStatement statement2 = connection.prepareStatement(readMediasCltQuery);
            
            statement2.setInt(1, id);
            statement2.setDate(2, d1);
            statement2.setDate(3, d2);
            
            ResultSet result2 = statement2.executeQuery();
            
            Historico2 hist2 = new Historico2();
            
            if (result2.next()) {
                hist2.setValor_medio(result2.getDouble("valor_medio"));
                hist2.setNota_media_do_clt(result2.getInt("nota_media_do_clt"));
                hist2.setMedia_distancia(result2.getDouble("media_distancia"));
                hist2.setNota_media_do_tax(result2.getInt("nota_media_do_tax"));
            }
            
            return hist2;
        } catch (SQLException ex) {
            Logger.getLogger(UsuarioDAO.class.getName()).log(Level.SEVERE, null, ex);
        }
        return null;
    }
    
    public List<Historico> readForTaxLista(int id, Date d1, Date d2) {
        try {
            PreparedStatement statement = connection.prepareStatement(readInfoTaxQuery);
            
            statement.setInt(1, id);
            statement.setDate(2, d1);
            statement.setDate(3, d2);
            
            ResultSet result = statement.executeQuery();
            
            List<Historico> lista = new ArrayList<>();
            Historico hist;
            
            while (result.next()) {
                hist = new Historico();
                
                hist.setDistancia(result.getDouble("distancia"));
                hist.setValor(result.getDouble("valor"));
                hist.setNota_do_clt(result.getInt("nota_do_clt"));
                hist.setData(result.getDate("dataa"));
                hist.setClt_pname(result.getString("clt_pname"));
                hist.setClt_sname(result.getString("clt_sname"));
                
                lista.add(hist);
            }
            
            return lista;
        } catch (SQLException ex) {
            Logger.getLogger(UsuarioDAO.class.getName()).log(Level.SEVERE, null, ex);
        }
        return null;
    }
    
    public Historico2 readForTaxMedias(int id, Date d1, Date d2) {
        try {
            PreparedStatement statement2 = connection.prepareStatement(readMediasTaxQuery);
            
            statement2.setInt(1, id);
            statement2.setDate(2, d1);
            statement2.setDate(3, d2);
            
            ResultSet result2 = statement2.executeQuery();
            
            Historico2 hist2 = new Historico2();
            
            if (result2.next()) {
                hist2.setValor_medio(result2.getDouble("valor_medio"));
                hist2.setNota_media_do_clt(result2.getInt("nota_media_do_clt"));
                hist2.setMedia_distancia(result2.getDouble("media_distancia"));
                hist2.setNota_media_do_tax(result2.getInt("nota_media_do_tax"));
            }
            
            return hist2;
        } catch (SQLException ex) {
            Logger.getLogger(UsuarioDAO.class.getName()).log(Level.SEVERE, null, ex);
        }
        return null;
    }

    @Override
    public void update(Historico obj) {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public void delete(int id) {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public List<Historico> all() {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public Historico read(int id) {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }
    
}
