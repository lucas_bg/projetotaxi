package dao;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;
import model.Veiculo;

public class VeiculoDAO extends DAO<Veiculo> {
    
    private final String createQuery = "INSERT INTO veiculo(renavam, ano, cor, placa, marca, modelo, cap, id_usuario, foto) "
            + "VALUES (?, ?, ?, ?, ?, ?, ?, ?, ?) RETURNING id;";
    
    private final String readQuery = "SELECT * FROM veiculo WHERE id = ?;";

    private final String readRenavamQuery = "SELECT * FROM veiculo WHERE renavam = ?;";

    private final String readUserIdQuery = "SELECT * FROM veiculo WHERE id_usuario = ?;";
    
    private final String updateQuery = "UPDATE veiculo "
                                     + "SET renavam = ?, ano = ?, cor = ?, placa = ?, "
                                     + "marca = ?, modelo = ?, cap = ?, id_usuario = ?, foto = ? "
                                     + "WHERE id = ?;";
    
    private final String updateByUserQuery = "UPDATE veiculo "
                                     + "SET renavam = ?, ano = ?, cor = ?, placa = ?, "
                                     + "marca = ?, modelo = ?, cap = ?, foto = ? "
                                     + "WHERE id_usuario = ?;";
    
    private final String deleteQuery  = "DELETE FROM veiculo WHERE id = ?;";

    private final String deleteByUserIdQuery  = "DELETE FROM veiculo WHERE id_usuario = ?;";
    
    private final String allQuery = "SELECT id, modelo FROM veiculo;";
    
    public VeiculoDAO(Connection connection) {
        super(connection);
    }
    
    @Override
    public void create(Veiculo veiculo) {
        try {
            PreparedStatement statement = connection.prepareStatement(createQuery);
            
            statement.setString(1, veiculo.getRenavam());
            statement.setString(2, veiculo.getAno());
            statement.setString(3, veiculo.getCor());
            statement.setString(4, veiculo.getPlaca());
            statement.setString(5, veiculo.getMarca());
            statement.setString(6, veiculo.getModelo());
            statement.setInt(7, veiculo.getCap());
            statement.setInt(8, veiculo.getId_usuario());
            statement.setBinaryStream(9, veiculo.getFoto(), veiculo.getFotoSize());

            ResultSet result = statement.executeQuery();
            
            if (result.next()) {
                veiculo.setId(result.getInt("id"));
            }
        } catch (SQLException ex) {
            Logger.getLogger(VeiculoDAO.class.getName()).log(Level.SEVERE, null, ex);
        }
    }

    @Override
    public Veiculo read(int id) {
        try {
            PreparedStatement statement = connection.prepareStatement(readQuery);
            
            statement.setInt(1, id);
            
            ResultSet result = statement.executeQuery();
            
            Veiculo veiculo = new Veiculo();
            
            if (result.next()) {
                veiculo.setRenavam(result.getString("renavam"));
                veiculo.setAno(result.getString("ano"));
                veiculo.setCor(result.getString("cor"));
                veiculo.setPlaca(result.getString("modelo"));
                veiculo.setMarca(result.getString("marca"));
                veiculo.setModelo(result.getString("modelo"));
                veiculo.setCap(result.getInt("cap"));
                veiculo.setId_usuario(result.getInt("id_usuario"));
                veiculo.setFoto(result.getBinaryStream("foto"));
                veiculo.setId(id);
            }
            
            return veiculo;
        } catch (SQLException ex) {
            Logger.getLogger(VeiculoDAO.class.getName()).log(Level.SEVERE, null, ex);
        }
        
        return null;
    }

    public Veiculo readUserId(int id) {
        try {
            PreparedStatement statement = connection.prepareStatement(readUserIdQuery);
            
            statement.setInt(1, id);
            
            ResultSet result = statement.executeQuery();
            
            Veiculo veiculo = new Veiculo();
            
            if (result.next()) {
                veiculo.setRenavam(result.getString("renavam"));
                veiculo.setAno(result.getString("ano"));
                veiculo.setCor(result.getString("cor"));
                veiculo.setPlaca(result.getString("modelo"));
                veiculo.setMarca(result.getString("marca"));
                veiculo.setModelo(result.getString("modelo"));
                veiculo.setCap(result.getInt("cap"));
                veiculo.setId_usuario(result.getInt("id_usuario"));
                veiculo.setFoto(result.getBinaryStream("foto"));
                veiculo.setId(result.getInt("id"));
            }
            
            return veiculo;
        } catch (SQLException ex) {
            Logger.getLogger(VeiculoDAO.class.getName()).log(Level.SEVERE, null, ex);
        }
        
        return null;
    }
    
    public Veiculo readRenavam(String id) {
        try {
            PreparedStatement statement = connection.prepareStatement(readRenavamQuery);
            
            statement.setString(1, id);
            
            ResultSet result = statement.executeQuery();
            
            Veiculo veiculo = new Veiculo();
            
            if (result.next()) {
                veiculo.setRenavam(result.getString("renavam"));
                veiculo.setAno(result.getString("ano"));
                veiculo.setCor(result.getString("cor"));
                veiculo.setPlaca(result.getString("modelo"));
                veiculo.setMarca(result.getString("marca"));
                veiculo.setModelo(result.getString("modelo"));
                veiculo.setCap(result.getInt("cap"));
                veiculo.setId_usuario(result.getInt("id_usuario"));
                veiculo.setFoto(result.getBinaryStream("foto"));
                veiculo.setId(result.getInt("id"));
            }
            
            return veiculo;
        } catch (SQLException ex) {
            Logger.getLogger(VeiculoDAO.class.getName()).log(Level.SEVERE, null, ex);
        }
        
        return null;
    }
    
    @Override
    public void update(Veiculo veiculo) {
        try {
            PreparedStatement statement = connection.prepareStatement(updateQuery);
            
            statement.setString(1, veiculo.getRenavam());
            statement.setString(2, veiculo.getAno());
            statement.setString(3, veiculo.getCor());
            statement.setString(4, veiculo.getPlaca());
            statement.setString(5, veiculo.getMarca());
            statement.setString(6, veiculo.getModelo());
            statement.setInt(7, veiculo.getCap());
            statement.setInt(8, veiculo.getId_usuario());
            statement.setBinaryStream(9, veiculo.getFoto(), veiculo.getFotoSize());
            
            statement.setInt(10, veiculo.getId());
            
            statement.executeUpdate();
        } catch (SQLException ex) {
            Logger.getLogger(VeiculoDAO.class.getName()).log(Level.SEVERE, null, ex);
        }
    }

    public void updateByUser(Veiculo veiculo) {
        try {
            PreparedStatement statement = connection.prepareStatement(updateByUserQuery);
            
            statement.setString(1, veiculo.getRenavam());
            statement.setString(2, veiculo.getAno());
            statement.setString(3, veiculo.getCor());
            statement.setString(4, veiculo.getPlaca());
            statement.setString(5, veiculo.getMarca());
            statement.setString(6, veiculo.getModelo());
            statement.setInt(7, veiculo.getCap());
            statement.setBinaryStream(8, veiculo.getFoto(), veiculo.getFotoSize());
            
            statement.setInt(9, veiculo.getId_usuario());
                        
            statement.executeUpdate();
        } catch (SQLException ex) {
            Logger.getLogger(VeiculoDAO.class.getName()).log(Level.SEVERE, null, ex);
        }
    }
    
    @Override
    public void delete(int id) {
        try {
            PreparedStatement statement = connection.prepareStatement(deleteQuery);
            
            statement.setInt(1, id);
            
            statement.executeUpdate();
        } catch (SQLException ex) {
            Logger.getLogger(VeiculoDAO.class.getName()).log(Level.SEVERE, null, ex);
        }
    }
    
    public void deleteByUserId(int id) {
        try {
            PreparedStatement statement = connection.prepareStatement(deleteByUserIdQuery);
            
            statement.setInt(1, id);
            
            statement.executeUpdate();
        } catch (SQLException ex) {
            Logger.getLogger(VeiculoDAO.class.getName()).log(Level.SEVERE, null, ex);
        }
    }

    @Override
    public List<Veiculo> all() {
        try {
            PreparedStatement statement = connection.prepareStatement(allQuery);
            
            ResultSet result = statement.executeQuery();
            
            List<Veiculo> veiculos = new ArrayList<>();
            
            Veiculo veiculo;
            
            while (result.next()) {
                veiculo = new Veiculo();
                
                veiculo.setId(result.getInt("id"));
                veiculo.setModelo(result.getString("modelo"));
                
                veiculos.add(veiculo);
            }
            
            return veiculos;
        } catch (SQLException ex) {
            Logger.getLogger(VeiculoDAO.class.getName()).log(Level.SEVERE, null, ex);
        }
        
        return null;
    }
}
