package dao;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;
import model.Usuario;

public class UsuarioDAO extends DAO<Usuario> {
    
    private final String createQuery = "INSERT INTO usuario(login, senha, pnome, snome, tel, cpf, sexo, ativo, dtNasc, foto) "
            + "VALUES (?, md5(?), ?, ?, ?, ?, ?, ?, ?, ?) RETURNING id;";
    
    private final String readQuery = "SELECT * FROM usuario WHERE id = ?;";
    
    private final String updateQuery = "UPDATE usuario "
                                     + "SET login = ?, senha = md5(?), pnome = ?, snome = ?, dtNasc = ?,"
                                     + " tel = ?, sexo = ?, ativo = ?, cpf = ?, foto = ? "
                                     + "WHERE id = ?;";
    
    private final String deleteQuery  = "DELETE FROM usuario WHERE id = ?;";
    
    private final String allQuery = "SELECT id, pnome FROM usuario;";
    
    private final String authenticateQuery = "SELECT * "
                                           + "FROM usuario "
                                            + "WHERE login = ? AND senha = md5(?);";

    public UsuarioDAO(Connection connection) {
        super(connection);
    }
    
    @Override
    public void create(Usuario usuario) {
        try {
            PreparedStatement statement = connection.prepareStatement(createQuery);
            
            statement.setString(1, usuario.getLogin());
            statement.setString(2, usuario.getSenha());
            statement.setString(3, usuario.getPnome());
            statement.setString(4, usuario.getSnome());
            statement.setString(5, usuario.getTel());
            statement.setString(6, usuario.getCpf());
            statement.setString(7, usuario.getSexo());
            statement.setBoolean(8, usuario.isAtivo());
            statement.setDate(9, usuario.getDtNasc());
            statement.setBinaryStream(10, usuario.getFoto(), usuario.getFotoSize());
                        
            ResultSet result = statement.executeQuery();
            
            if (result.next()) {
                usuario.setId(result.getInt("id"));
            }
        } catch (SQLException ex) {
            Logger.getLogger(UsuarioDAO.class.getName()).log(Level.SEVERE, null, ex);
        }
    }

    @Override
    public Usuario read(int id) {
        try {
            PreparedStatement statement = connection.prepareStatement(readQuery);
            
            statement.setInt(1, id);
            
            ResultSet result = statement.executeQuery();
            
            Usuario usuario = new Usuario();
            
            if (result.next()) {
                usuario.setId(result.getInt("id"));
                usuario.setLogin(result.getString("login"));
                usuario.setSenha(result.getString("senha"));
                usuario.setPnome(result.getString("pnome"));
                usuario.setSnome(result.getString("snome"));
                usuario.setTel(result.getString("tel"));
                usuario.setSexo(result.getString("sexo"));
                usuario.setAtivo(result.getBoolean("ativo"));
                usuario.setCpf(result.getString("cpf"));
                usuario.setDtNasc(result.getDate("dtNasc"));
                usuario.setFoto(result.getBinaryStream("foto"));
            }
            
            return usuario;
        } catch (SQLException ex) {
            Logger.getLogger(UsuarioDAO.class.getName()).log(Level.SEVERE, null, ex);
        }
        
        return null;
    }

    @Override
    public void update(Usuario usuario) {
        try {
            PreparedStatement statement = connection.prepareStatement(updateQuery);
            
            statement.setString(1, usuario.getLogin());
            statement.setString(2, usuario.getSenha());
            statement.setString(3, usuario.getPnome());
            statement.setString(4, usuario.getSnome());
            statement.setDate(5, usuario.getDtNasc());
            statement.setString(6, usuario.getTel());
            statement.setString(7, usuario.getSexo());
            statement.setBoolean(8, usuario.isAtivo());
            statement.setString(9, usuario.getCpf());
            statement.setBinaryStream(10, usuario.getFoto(), usuario.getFotoSize());
            
            statement.setInt(11, usuario.getId());
                        
            statement.executeUpdate();
        } catch (SQLException ex) {
            Logger.getLogger(UsuarioDAO.class.getName()).log(Level.SEVERE, null, ex);
        }
    }

    @Override
    public void delete(int id) {
        try {
            PreparedStatement statement = connection.prepareStatement(deleteQuery);
            
            statement.setInt(1, id);
            
            statement.executeUpdate();
        } catch (SQLException ex) {
            Logger.getLogger(UsuarioDAO.class.getName()).log(Level.SEVERE, null, ex);
        }
    }

    @Override
    public List<Usuario> all() {
        try {
            PreparedStatement statement = connection.prepareStatement(allQuery);
            
            ResultSet result = statement.executeQuery();
            
            List<Usuario> usuarios = new ArrayList<>();
            
            Usuario usuario;
            
            while (result.next()) {
                usuario = new Usuario();
                
                usuario.setId(result.getInt("id"));
                usuario.setPnome(result.getString("pnome"));
                
                usuarios.add(usuario);
            }
            
            return usuarios;
        } catch (SQLException ex) {
            Logger.getLogger(UsuarioDAO.class.getName()).log(Level.SEVERE, null, ex);
        }
        
        return null;
    }

    public Usuario authenticate(String login, String password) {
         try {
            PreparedStatement statement = connection.prepareStatement(authenticateQuery);
            
            statement.setString(1, login);
            statement.setString(2, password);
            
            ResultSet result  = statement.executeQuery();
            
            if (result.next()) {
                Usuario usuario = new Usuario();
                usuario.setId(result.getInt("id"));
                usuario.setLogin(result.getString("login"));
                usuario.setSenha(result.getString("senha"));
                usuario.setPnome(result.getString("pnome"));
                usuario.setSnome(result.getString("snome"));
                usuario.setTel(result.getString("tel"));
                usuario.setSexo(result.getString("sexo"));
                usuario.setAtivo(result.getBoolean("ativo"));
                usuario.setCpf(result.getString("cpf"));
                usuario.setDtNasc(result.getDate("dtNasc"));
                usuario.setFoto(result.getBinaryStream("foto"));
                return usuario;
            }
        } catch (SQLException ex) {
            Logger.getLogger(UsuarioDAO.class.getName()).log(Level.SEVERE, null, ex);
        }
         
        return null;
    }
    
}
