<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>

<nav class="navbar navbar-inverse navbar-static-top">
    <div class="container-fluid">
        <div class="navbar-header">
            <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#navbar" aria-expanded="false" aria-controls="navbar">
                <span class="sr-only">Toggle navigation</span>
                <span class="icon-bar"></span>
                <span class="icon-bar"></span>
                <span class="icon-bar"></span>
            </button>
            <a class="navbar-brand" href="#">KASTAXI</a>
        </div>
        <div id="navbar" class="navbar-collapse collapse">
            <div class="navbar right">
                <li><a href="${pageContext.servletContext.contextPath}/">�nicio</a>

                <c:choose>
                    <c:when test="${not empty sessionScope.usuario}" >
                        <a href="${pageContext.servletContext.contextPath}/logout" />Logout</a>
                    </c:when>
                    <c:otherwise>
                        <a href="${pageContext.servletContext.contextPath}/usuario/create">Cadastro</a>
                        <a href="${pageContext.servletContext.contextPath}/login" />Login</a>
                    </c:otherwise>
                </c:choose>
                </li>
            </div>
        </div>
    </div>
</nav>
