<%@page contentType="text/html" pageEncoding="UTF-8"%>

<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>

<c:if test="${empty sessionScope.usuario}">
    <c:redirect context="${pageContext.servletContext.contextPath}" url="/" ></c:redirect>
</c:if>

<!DOCTYPE html>
<html>
    <head>
        <%@include file="/view/includes/head.jsp" %>
    </head>
    <body>
        <div class="container">
            <div class="row">
                <div class="col-md-10 col-xs-10">
                    <%@include file="/view/includes/menu.jsp" %>
                </div>
            </div>

            <div class="row">
                <div class="col-md-10">
                    
                    <li><p>Renavam: ${veiculo.renavam} </p></li>
                    <li><p>Ano: ${veiculo.ano} </p></li>
                    <li><p>Cor: ${veiculo.cor} </p></li>
                    <li><p>Placa: ${veiculo.placa} </p></li>
                    <li><p>Modelo: ${veiculo.modelo} </p></li>
                    <li><p>Capacidade: ${veiculo.cap} </p></li>
                    
                    <li><p>FOTO: <img style="max-width: 80px; height: auto; " src=${urlFotoCarro} /></p></li>
                    
                </div>
            </div>
            <%@include file="/view/includes/rodape.jsp" %>
        </div>
        <%@include file="/view/includes/scripts.jsp" %>
    </body>
</html>
