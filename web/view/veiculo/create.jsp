<%@page contentType="text/html" pageEncoding="UTF-8"%>

<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>

<c:if test="${empty sessionScope.usuario}">
    <c:redirect context="${pageContext.servletContext.contextPath}" url="/" ></c:redirect>
</c:if>

<!DOCTYPE html>
<html>
    <head>
        <title>Create</title>
        <%@include file="/view/includes/head.jsp" %>
    </head>
    <body>
        <div class="container">
            <div class="row">
                <div class="col-md-10 col-xs-10">
                    <%@include file="/view/includes/menu.jsp" %>
                    <h1>Cadastro de Veiculo </h1>
                    <form method="post" action="${pageContext.servletContext.contextPath}/veiculo/create"  enctype="multipart/form-data"  >

                        <div class="form-group">
                            <label>Renavam: </label>
                            <input type="text" name="renavam" class="form-control" />
                        </div>

                        <div class="form-group">
                            <label>Ano: </label>
                            <input type="text" name="ano" class="form-control" />
                        </div>

                        <div class="form-group">
                            <label>Cor: </label>
                            <input type="text" name="cor" class="form-control" />
                        </div>

                        <div class="form-group">
                            <label>Placa: </label>
                            <input type="text" name="placa" class="form-control" />
                        </div>

                        <div class="form-group">
                            <label>Marca: </label>
                            <input type="text" name="marca" class="form-control" />
                        </div>
                        
                        <div class="form-group">
                            <label>Modelo: </label>
                            <input type="text" name="modelo" class="form-control" />
                        </div>

                        <div class="form-group">
                            <label>Capacidade de passageiros: </label>
                            <input type="text" name="cap" class="form-control" />
                        </div>
                        
                        <div class="form-group">
                            <label>Foto:</label>
                            <input type="file" name="foto" class="form-control" />
                        </div>
                        
                        <div class="form-group">
                            <label>Aproveite e defina seu preco: </label>
                            <input type="text" name="preco" class="form-control" />
                        </div>

                        <input type="submit" value="Enviar" class="btn btn-primary" />
                    </form>
                    <%@include file="/view/includes/rodape.jsp" %>
                </div>
            </div>
            <%@include file="/view/includes/scripts.jsp" %>
        </div>
    </body>
</html>
