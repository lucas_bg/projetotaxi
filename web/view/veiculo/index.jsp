<%@page contentType="text/html" pageEncoding="UTF-8"%>

<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>

<c:if test="${empty sessionScope.usuario}">
    <c:redirect context="${pageContext.servletContext.contextPath}" url="/" ></c:redirect>
</c:if>

<!DOCTYPE html>
<html>
    <head>
        <%@include file="/view/includes/head.jsp" %>
    </head>
    <body>
        <div class="container">
            <div class="row">
                <div class="col-md-10 col-xs-10">
                    <%@include file="/view/includes/menu.jsp" %>
                </div>
            </div>

            <div class="row">
                <div class="col-md-10">
                    <p> Seja bem vindo ${usuario.pnome} ${usuario.snome} </p>
                    <br>
                    
                    <a href="${pageContext.servletContext.contextPath}/veiculo/create" /> Cadastrar Veiculo </a>
                    <br>
                    <a href="${pageContext.servletContext.contextPath}/veiculo/update" /> Atualizar Veiculo </a>
                    <br>
                    <a href="${pageContext.servletContext.contextPath}/veiculo/read"> Ver meu veiculo </a>
                    <br>
                    <a href="${pageContext.servletContext.contextPath}/veiculo/delete"> Deletar meu veiculo </a>
                    <br>
                </div>
            </div>
            <%@include file="/view/includes/rodape.jsp" %>
        </div>
        <%@include file="/view/includes/scripts.jsp" %>
    </body>
</html>
