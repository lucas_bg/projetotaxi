<%@page contentType="text/html" pageEncoding="UTF-8"%>

<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>

<c:if test="${empty sessionScope.usuario}">
    <c:redirect context="${pageContext.servletContext.contextPath}" url="/" ></c:redirect>
</c:if>

<!DOCTYPE html>
<html>
    <head>
        <%@include file="/view/includes/head.jsp" %>
    </head>
    <body>
        <div class="container">

            <div class="row">
                <div class="col-md-10 col-xs-10">
                    <%@include file="/view/includes/menu.jsp" %>
                </div>
            </div>
                
            <form method="post" action="${pageContext.servletContext.contextPath}/usuario/estGlob" >

                <p> Datas no formato dd/MM/yyyy. Caso queira saber de todo o periodo, deixar em branco </p>
                
                <div class="form-group">
                    <label> Data inicial: </label>
                    <input type="text" name="dataInicial" class="form-control" />
                </div>
                
                <div class="form-group">
                    <label> Data final: </label>
                    <input type="text" name="dataFinal" class="form-control" />
                </div>

                <input type="submit" value="Ver" class="btn btn-default" />
            </form>
            
            <p> Total de corridas: ${query1_1.total_corr} </p>
            <p> Distância média percorrida: ${query1_1.dist_media} Km </p>
            <p> Distância mínima percorrida: ${query1_1.dist_min} Km </p>
            <p> Distância máxima percorrida: ${query1_1.dist_max} Km </p>
            <p> Média do valor total da corrida: R$ ${query1_1.valor_media} </p>
            <p> Maior valor total da corrida: R$ ${query1_1.valor_max} </p>
            <p> Menor valor total da corrida: R$ ${query1_1.valor_min} </p>
            <p> Média do valor por kilometro rodado: R$ ${query1_1.valor_cpkr_media} </p>
            <p> Maior valor por kilometro rodado: R$ ${query1_1.valor_cpkr_max} </p>
            <p> Menor valor por kilometro rodado: R$ ${query1_1.valor_cpkr_min} </p>
            
            <p> SOBRE VEICULOS - POR MARCA </p>
            <div class="row">
                <div class="col-md-10">
                    <table class="table table-striped table-bordered table-hover">
                        <thead>
                            <tr>
                                <th> Quantidade total </th>
                                <th> Valor médio por kilometro rodado </th>
                                <th> Valor mínimo por kilometro rodado </th>
                                <th> Valor máximo por kilometro rodado </th>
                            </tr>
                        </thead>
                        <tbody>
                            <c:forEach items="${query2_1}" var="el">
                                <tr>
                                    <td> ${el.qtdade_total_corr_marca} </td>
                                    <td> ${el.valor_cpkr_media_marca} </td>
                                    <td> ${el.valor_cpkr_min_marca} </td>
                                    <td> ${el.valor_cpkr_max_marca} </td>
                                </tr>
                            </c:forEach>
                        </tbody>
                    </table>
                </div>
            </div>
            <p> SOBRE VEICULOS - POR MODELO </p>
            <div class="row">
                <div class="col-md-10">
                    <table class="table table-striped table-bordered table-hover">
                        <thead>
                            <tr>
                                <th> Quantidade total </th>
                                <th> Valor médio por kilometro rodado </th>
                                <th> Valor mínimo por kilometro rodado </th>
                                <th> Valor máximo por kilometro rodado </th>
                            </tr>
                        </thead>
                        <tbody>
                            <c:forEach items="${query2_2}" var="el">
                                <tr>
                                    <td> ${el.qtdade_total_corr_modelo} </td>
                                    <td> ${el.valor_cpkr_media_modelo} </td>
                                    <td> ${el.valor_cpkr_min_modelo} </td>
                                    <td> ${el.valor_cpkr_max_modelo} </td>
                                </tr>
                            </c:forEach>
                        </tbody>
                    </table>
                </div>
            </div>
            <p> TOP 10 em quantidade de corridas (Clientes) </p>
            <div class="row">
                <div class="col-md-10">
                    <table class="table table-striped table-bordered table-hover">
                        <thead>
                            <tr>
                                <th> Nome </th>
                                <th> Quantidade </th>
                            </tr>
                        </thead>
                        <tbody>
                            <c:forEach items="${query3_1}" var="el">
                                <tr>
                                    <td> ${el.top_10_qtdade_corridas_cliente_nome} </td>
                                    <td> ${el.top_10_qtdade_corridas_cliente_qtdade} </td>
                                </tr>
                            </c:forEach>
                        </tbody>
                    </table>
                </div>
            </div>
            <p> TOP 10 em quantidade de corridas (Taxistas) </p>
            <div class="row">
                <div class="col-md-10">
                    <table class="table table-striped table-bordered table-hover">
                        <thead>
                            <tr>
                                <th> Nome </th>
                                <th> Quantidade </th>
                            </tr>
                        </thead>
                        <tbody>
                            <c:forEach items="${query3_2}" var="el">
                                <tr>
                                    <td> ${el.top_10_qtdade_corridas_taxista_nome} </td>
                                    <td> ${el.top_10_qtdade_corridas_taxista_qtdade} </td>
                                </tr>
                            </c:forEach>
                        </tbody>
                    </table>
                </div>
            </div>
            <p> TOP 10 em valor total pago </p>
            <div class="row">
                <div class="col-md-10">
                    <table class="table table-striped table-bordered table-hover">
                        <thead>
                            <tr>
                                <th> Nome </th>
                                <th> Valor (R$) </th>
                            </tr>
                        </thead>
                        <tbody>
                            <c:forEach items="${query3_3}" var="el">
                                <tr>
                                    <td> ${el.top_10_soma_valores_cliente_nome} </td>
                                    <td> ${el.top_10_soma_valores_cliente_soma} </td>
                                </tr>
                            </c:forEach>
                        </tbody>
                    </table>
                </div>
            </div>
            <p> TOP 10 em valor total recebido </p>
            <div class="row">
                <div class="col-md-10">
                    <table class="table table-striped table-bordered table-hover">
                        <thead>
                            <tr>
                                <th> Nome </th>
                                <th> Valor (R$) </th>
                            </tr>
                        </thead>
                        <tbody>
                            <c:forEach items="${query3_4}" var="el">
                                <tr>
                                    <td> ${el.top_10_soma_valores_taxista_nome} </td>
                                    <td> ${el.top_10_soma_valores_taxista_soma} </td>
                                </tr>
                            </c:forEach>
                        </tbody>
                    </table>
                </div>
            </div>
            <p> TOP 10 em média de nota de satisfação (Notas atribuidas a clientes) </p>
            <div class="row">
                <div class="col-md-10">
                    <table class="table table-striped table-bordered table-hover">
                        <thead>
                            <tr>
                                <th> Nome </th>
                                <th> Nota </th>
                            </tr>
                        </thead>
                        <tbody>
                            <c:forEach items="${query3_5}" var="el">
                                <tr>
                                    <td> ${el.top_10_media_nota_cliente_nome} </td>
                                    <td> ${el.top_10_media_nota_cliente_media} </td>
                                </tr>
                            </c:forEach>
                        </tbody>
                    </table>
                </div>
            </div>
            <p> TOP 10 em média de nota de satisfação (Notas atribuidas a taxistas) </p>
            <div class="row">
                <div class="col-md-10">
                    <table class="table table-striped table-bordered table-hover">
                        <thead>
                            <tr>
                                <th> Nome </th>
                                <th> Nota </th>
                            </tr>
                        </thead>
                        <tbody>
                            <c:forEach items="${query3_6}" var="el">
                                <tr>
                                    <td> ${el.top_10_media_nota_taxista_nome} </td>
                                    <td> ${el.top_10_media_nota_taxista_media} </td>
                                </tr>
                            </c:forEach>
                        </tbody>
                    </table>
                </div>
            </div>
            <p> TOP 10 Clientes Fidelidade </p>
            <div class="row">
                <div class="col-md-10">
                    <table class="table table-striped table-bordered table-hover">
                        <thead>
                            <tr>
                                <th> Login Cliente </th>
                                <th> Login Taxista </th>
                                <th> Quantidade de vezes </th>
                            </tr>
                        </thead>
                        <tbody>
                            <c:forEach items="${query4_1}" var="el">
                                <tr>
                                    <td> ${el.top_10_cliente_fiel_qt_vezes} </td>
                                    <td> ${el.top_10_cliente_fiel_login_clt} </td>
                                    <td> ${el.top_10_cliente_fiel_login_tax} </td>
                                </tr>
                            </c:forEach>
                        </tbody>
                    </table>
                </div>
            </div>
                
        </div>
    </body>
</html>
