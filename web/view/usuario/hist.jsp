<%@page contentType="text/html" pageEncoding="UTF-8"%>

<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>

<c:if test="${empty sessionScope.usuario}">
    <c:redirect context="${pageContext.servletContext.contextPath}" url="/" ></c:redirect>
</c:if>

<!DOCTYPE html>
<html>
    <head>
        <%@include file="/view/includes/head.jsp" %>
    </head>
    <body>
        <div class="container">

            <div class="row">
                <div class="col-md-10 col-xs-10">
                    <%@include file="/view/includes/menu.jsp" %>
                </div>
            </div>
                
            <form method="post" action="${pageContext.servletContext.contextPath}/usuario/historico" >
                
                <p> Datas no formato dd/MM/yyyy. Caso queira saber de todo o periodo, deixar em branco </p>

                <div class="form-group">
                    <label> Data inicial: </label>
                    <input type="text" name="dataInicial" class="form-control" />
                </div>
                
                <div class="form-group">
                    <label> Data final: </label>
                    <input type="text" name="dataFinal" class="form-control" />
                </div>

                <input type="submit" value="Ver" class="btn btn-default" />
            </form>
            
            <p> Valor médio por corrida: R$ ${mediasHistCorr.valor_medio} </p>
            <p> Média das notas atribuídas a clientes: ${mediasHistCorr.nota_media_do_clt} </p>
            <p> Distância média percorrida: ${mediasHistCorr.media_distancia} Km </p>
            <p> Média das notas atribuídas a taxistas: ${mediasHistCorr.nota_media_do_tax} </p>
                
            <div class="row">
                <div class="col-md-10">
                    <table class="table table-striped table-bordered table-hover">
                        <thead>
                            <tr>
                                <th> Distancia </th>
                                <th> Valor </th>
                                <th> Data </th>
                                <c:choose>
                                <c:when test="${isTaxista}">
                                    <th> Nota atribuida ao cliente </th>
                                    <th> Nome do cliente </th>
                                </c:when>
                                <c:otherwise>
                                    <th> Nota atribuida ao taxista </th>
                                    <th> Nome do taxista </th>
                                </c:otherwise>
                                </c:choose>
                            </tr>
                        </thead>
                        <tbody>
                            <c:forEach items="${listaHistoricos}" var="corrida">
                                <tr>
                                    <td> ${corrida.distancia} </td>
                                    <td> ${corrida.valor} </td>
                                    <td> ${corrida.data} </td>
                                    <c:choose>
                                    <c:when test="${isTaxista}">
                                        <td> ${corrida.nota_do_clt} </td>
                                        <td> ${corrida.clt_pname} ${corrida.clt_sname} </td>
                                    </c:when>
                                    <c:otherwise>
                                        <td> ${corrida.nota_do_tax} </td>
                                        <td> ${corrida.tax_pname} ${corrida.tax_sname} </td>
                                    </c:otherwise>
                                    </c:choose>
                                </tr>
                            </c:forEach>
                        </tbody>
                    </table>
                </div>
            </div>

        </div>
    </body>
</html>
