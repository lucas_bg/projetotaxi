<%@page contentType="text/html" pageEncoding="UTF-8"%>

<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>

<c:if test="${empty sessionScope.usuario}">
    <c:redirect context="${pageContext.servletContext.contextPath}" url="/" ></c:redirect>
</c:if>

<!DOCTYPE html>
<html>
    <head>
        <title>Update</title>
        <%@include file="/view/includes/head.jsp" %>
    </head>
    <body>
        <div class="container">
            <div class="row">
                <div class="col-md-10 col-xs-10">
                    <%@include file="/view/includes/menu.jsp" %>
                    <h1>Atualizar usuário</h1>
                </div>
            </div>

            <form method="post" action="${pageContext.servletContext.contextPath}/usuario/update" enctype="multipart/form-data" >

                <input type="hidden" name="id" value="${usuario.id}" />

                <div class="form-group">
                    <label>Login: </label>
                    <input type="text" name="login" class="form-control" value="${usuario.login}" />
                </div>

                <div class="form-group">
                    <label>Senha: </label>
                    <input type="password" name="senha" class="form-control" />
                </div>

                <div class="form-group">
                    <label>Nome:</label>
                    <input type="text" name="pnome" class="form-control" value="${usuario.pnome}" />
                </div>

                <div class="form-group">
                    <label>Sobrenome:</label>
                    <input type="text" name="snome" class="form-control" value="${usuario.snome}" />
                </div>

                <div class="form-group">
                    <label>Data nascimento:</label>
                    <input type="date" name="dataNasc" class="form-control" id="data" />
                </div>

                <div class="form-group">
                    <label>Telefone:</label>
                    <input type="text" name="tel" class="form-control" value="${usuario.tel}" />
                </div>

                <div class="form-group">
                    <label>Sexo (m ou f):</label>
                    <input type="text" name="sexo" class="form-control" />
                </div>

                <div class="form-group">
                    <label>CPF:</label>
                    <input type="text" name="cpf" class="form-control" value="${usuario.cpf}" />
                </div>

                <div class="form-group">
                    <label>Foto:</label>
                    <input type="file" name="foto" class="form-control" />
                </div>
                
                <c:if test="${ehTaxista}">
                    <div class="form-group">
                        <label>Preço:</label>
                        <input type="text" name="valor" class="form-control" />
                    </div>
                </c:if>
                
                <input type="submit" value="Atualizar" class="btn btn-primary" />
            </form>
            <%@include file="/view/includes/rodape.jsp" %>
            <%@include file="/view/includes/scripts.jsp" %>
        </div>
    </body>
</html>
