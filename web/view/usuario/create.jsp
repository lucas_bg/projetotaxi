<%@page contentType="text/html" pageEncoding="UTF-8"%>

<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>

<!DOCTYPE html>
<html>
    <head>
        <title>Create</title>
        <%@include file="/view/includes/head.jsp" %>
    </head>
    <body>
        <div class="container">
            <div class="row">
                <div class="col-md-10 col-xs-10">
                    <%@include file="/view/includes/menu.jsp" %>
                    <h1>Cadastro de usuário</h1>
                    <form method="post" action="${pageContext.servletContext.contextPath}/usuario/create"  enctype="multipart/form-data" >

                        <div class="form-group">
                            <label>Login: </label>
                            <input type="text" name="login" class="form-control" />
                        </div>

                        <div class="form-group">
                            <label>Senha: </label>
                            <input type="password" name="senha" class="form-control" />
                        </div>

                        <div class="form-group">
                            <label>Nome:</label>
                            <input type="text" name="pnome" class="form-control" />
                        </div>

                        <div class="form-group">
                            <label>Sobrenome:</label>
                            <input type="text" name="snome" class="form-control" />
                        </div>

                        <div class="form-group">
                            <label>Data nascimento (yyyy-MM-dd):</label>
                            <input type="date" name="dataNasc" class="form-control" />
                        </div>

                        <div class="form-group">
                            <label>Telefone:</label>
                            <input type="text" name="tel" class="form-control" />
                        </div>

                        <div class="form-group">
                            <label>Sexo (m ou f):</label>
                            <input type="text" name="sexo" class="form-control" />
                        </div>

                        <div class="form-group">
                            <label>CPF:</label>
                            <input type="text" name="cpf" class="form-control" />
                        </div>

                        <div class="form-group">
                            <label>Foto:</label>
                            <input type="file" name="foto" class="form-control" />
                        </div>
                        
                        <input type="submit" value="Enviar" class="btn btn-primary" />
                    </form>
                    <%@include file="/view/includes/rodape.jsp" %>
                </div>
            </div>

            <%@include file="/view/includes/scripts.jsp" %>
            <script>
                $( "#data" ).datepicker({  });
            </script>
        </div>
    </body>
</html>
