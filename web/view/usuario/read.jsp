<%@page contentType="text/html" pageEncoding="UTF-8"%>

<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>

<c:if test="${empty sessionScope.usuario}">
    <c:redirect context="${pageContext.servletContext.contextPath}" url="/" ></c:redirect>
</c:if>

<!DOCTYPE html>
<html>
    <head>
        <%@include file="/view/includes/head.jsp" %>
    </head>
    <body>
        <div class="container">

            <div class="row">
                <div class="col-md-10 col-xs-10">
                    <%@include file="/view/includes/menu.jsp" %>
                </div>
            </div>

            <div class="row">
                <div class="col-md-10">
                    <p> Seja bem vindo ${usuario.pnome} ${usuario.snome} </p>
                    <br>
                    <p> Suas informacoes: </p>
                    
                    <li><p>TELEFONE: ${usuario.tel}</p></li>
                    <li><p>SEXO: ${usuario.sexo}</p></li>
                    <li><p>CPF: ${usuario.cpf}</p></li>
                    <li><p>LOGIN: ${usuario.login}</p></li>
                    <li><p>DATA DE NASCIMENTO: ${usuario.dtNasc}</p></li>
                    <li><p>FOTO: <img style="max-width: 80px; height: auto; " src=${urlll} /></p></li>
                    
                    <c:if test="${ehTaxista}">
                        <li><p>VALOR: ${taxista.valor_cpkr}</p></li>
                    </c:if>
                    
                </div>
            </div>
            <%@include file="/view/includes/rodape.jsp" %>
        </div>
        <%@include file="/view/includes/scripts.jsp" %>
    </body>
</html>
