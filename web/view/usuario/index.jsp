<%@page contentType="text/html" pageEncoding="UTF-8"%>

<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>

<c:if test="${empty sessionScope.usuario}">
    <c:redirect context="${pageContext.servletContext.contextPath}" url="/" ></c:redirect>
</c:if>

<!DOCTYPE html>
<html>
    <head>
        <%@include file="/view/includes/head.jsp" %>
    </head>
    <body>
        <div class="container">

            <div class="row">
                <div class="col-md-10 col-xs-10">
                    <%@include file="/view/includes/menu.jsp" %>
                </div>
            </div>

            <div class="row">
                <div class="col-md-10">
                    <p> Seja bem vindo ${usuario.pnome} ${usuario.snome} </p>
                    <br>
                    <a href="${pageContext.servletContext.contextPath}/usuario/historico"> Veja seu historico de corridas! </a>
                    <br>
                    <a href="${pageContext.servletContext.contextPath}/usuario/estGlob"> Veja estatisticas globais! </a>
                    <br>
                    <a href="${pageContext.servletContext.contextPath}/veiculo"> Menu veiculo </a>
                    <br>
                    <a href="${pageContext.servletContext.contextPath}/usuario/read"> Read Cadastro </a>
                    <br>
                    <a href="${pageContext.servletContext.contextPath}/usuario/update"> Atualizar Cadastro </a>
                    <br>
                    <a href="${pageContext.servletContext.contextPath}/usuario/delete"> Deletar Cadastro </a>
                    <br>

                    <c:if test="${waitResp}">
                    <a href="${pageContext.servletContext.contextPath}/usuario/verifAct"> Clique para verificar se sua requisição foi aceita </a>
                    </c:if>
                    <c:if test="${corrRecusada}">
                    <a href="${pageContext.servletContext.contextPath}/usuario"> Sua requisição foi recusada! clique para atualizar </a>
                    </c:if>
                    <c:if test="${corrAceita}">
                    <a href="${pageContext.servletContext.contextPath}/corrida/comeca"> Sua requisição foi aceita! clique para começar a corrida </a>
                    </c:if>

                    <br>
                    
                    <form method="post" action="${pageContext.servletContext.contextPath}/usuario/reqCorr" >

                        <div class="form-group">
                            <label>Digite sua latitude atual: </label>
                            <input type="text" id="latEscCl2" name="lat" class="form-control" />
                        </div>
                        
                        <div class="form-group">
                            <label>Digite sua longitude atual: </label>
                            <input type="text" id="lngEscCl2" name="lng" class="form-control" />
                        </div>
                        
                        <input id="soPEsconder" type="submit" value="Requisitar" class="btn btn-primary" />
                    </form>


                    <input type="text" id="latEscCl" />
                    <input type="text" id="lngEscCl" />
                    
                    
                    <button class="btn btn-default" type="button" id="buttonTest" > Requisitar Corrida </button>

                    <br>
                    <br>

                    <div id="map">
                        
                    </div>
                    
                    
                </div>
            </div>
            <%@include file="/view/includes/rodape.jsp" %>
        </div>
        
        <%@include file="/view/includes/scripts.jsp" %>
        <c:choose>
        <c:when test="${isTaxista}">
            <script type="text/javascript" src="${pageContext.servletContext.contextPath}/assets/js/mapTaxista.js"></script>
        </c:when>
        <c:otherwise>
            <script type="text/javascript" src="${pageContext.servletContext.contextPath}/assets/js/map.js"></script>
        </c:otherwise>
        </c:choose>
        <script src="https://maps.googleapis.com/maps/api/js?key=AIzaSyB-BQI2fSVPRZ_vKa9DAUDv9VATDQfpd8k&callback=initMap" ></script>
    </body>
</html>
