<%@page contentType="text/html" pageEncoding="UTF-8"%>

<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>

<c:if test="${empty sessionScope.usuario}">
    <c:redirect context="${pageContext.servletContext.contextPath}" url="/" ></c:redirect>
</c:if>

<!DOCTYPE html>
<html>
    <head>
        <%@include file="/view/includes/head.jsp" %>
    </head>
    <body>
        <div class="container">

            <div class="row">
                <div class="col-md-10 col-xs-10">
                    <%@include file="/view/includes/menu.jsp" %>
                </div>
            </div>

            <div class="row">
                <div class="col-md-10">
                    
                    <c:choose>
                    <c:when test="${isTaxista}">
                        
                        <p> Ola taxista, aguarde a corrida acabar </p>
                        <p> Clique abaixo para atualizar </p>
                        <a href="${pageContext.servletContext.contextPath}/corrida/taxistaAguFim"> Atualizar </a>
                        
                    </c:when>
                    <c:otherwise>
                        
                        <c:if test="${aguardandoTax}" >
                            <p> Aguardadando taxista iniciar corrida, clique abaixo para atualizar </p>
                            <a href="${pageContext.servletContext.contextPath}/corrida/aguardandoTax"> Atualizar </a>
                        </c:if>
                        <c:if test="${readyForPart2}" >
                            <p> Corrida iniciada, clique abaixo para acompanhar </p>
                            <a href="${pageContext.servletContext.contextPath}/corrida/visualCorrPt2"> Acompanhar </a>
                        </c:if>
                        
                    </c:otherwise>
                    </c:choose>
                    
                    <div id="my-map">

                    </div>
                </div>
            </div>
            <%@include file="/view/includes/rodape.jsp" %>
        </div>
        <%@include file="/view/includes/scripts.jsp" %>
    </body>
</html>
