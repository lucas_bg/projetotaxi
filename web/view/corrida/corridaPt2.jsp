<%@page contentType="text/html" pageEncoding="UTF-8"%>

<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>

<c:if test="${empty sessionScope.usuario}">
    <c:redirect context="${pageContext.servletContext.contextPath}" url="/" ></c:redirect>
</c:if>

<!DOCTYPE html>
<html>
    <head>
        <%@include file="/view/includes/head.jsp" %>
    </head>
    <body>
        <div class="container">

            <div class="row">
                <div class="col-md-10 col-xs-10">
                    <%@include file="/view/includes/menu.jsp" %>
                </div>
            </div>

            <div class="row">
                <div class="col-md-10">
                    <p> Seja bem vindo ${usuario.pnome} ${usuario.snome} </p>
                    <br>
                    
                    <br>
                    <br>
                    
                    <br>
                    <c:choose>
                    <c:when test="${isTaxista}">
                        
                    </c:when>
                    <c:otherwise>
                        <form method="post" action="${pageContext.servletContext.contextPath}/corrida/atualizar2" >
                        
                        <input type="submit" value="Atualizar" class="btn btn-primary" />
                        
                        </form>
                    </c:otherwise>
                    </c:choose>
                    
                    
                    <div id="map">

                    </div>
                </div>
            </div>
            <%@include file="/view/includes/rodape.jsp" %>
        </div>
        <%@include file="/view/includes/scripts.jsp" %>
        <c:choose>
        <c:when test="${isTaxista}">
            
        </c:when>
        <c:otherwise>
            <script type="text/javascript" src="${pageContext.servletContext.contextPath}/assets/js/mapPt2.js"></script>
            <script src="https://maps.googleapis.com/maps/api/js?key=AIzaSyB-BQI2fSVPRZ_vKa9DAUDv9VATDQfpd8k&callback=initMap" async defer></script>
        </c:otherwise>
        </c:choose>
    </body>
</html>
