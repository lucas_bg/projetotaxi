<%@page contentType="text/html" pageEncoding="UTF-8"%>

<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>

<c:if test="${empty sessionScope.usuario}">
    <c:redirect context="${pageContext.servletContext.contextPath}" url="/" ></c:redirect>
</c:if>

<!DOCTYPE html>
<html>
    <head>
        <%@include file="/view/includes/head.jsp" %>
    </head>
    <body>
        <div class="container">

            <div class="row">
                <div class="col-md-10 col-xs-10">
                    <%@include file="/view/includes/menu.jsp" %>
                </div>
            </div>

            <div class="row">
                <div class="col-md-10">
                    <p> Seja bem vindo ${usuario.pnome} ${usuario.snome} </p>
                    <br>
                    
                    <br>
                    <br>
                    <c:if test="${isTaxista}">
                        <div class="row">
                            <div class="col-md-10">
                                <table class="table table-striped table-bordered table-hover">
                                    <thead>
                                        <tr>
                                            <th>Nome Cliente</th>
                                            <th>Telefone Cliente</th>
                                            <th>Sexo do Cliente</th>
                                            <th>Data Nascimento do Cliente</th>
                                            <th>Posição do Cliente (lat, long)</th>
                                            <th>Escolha</th>
                                            <th>Escolha</th>
                                        </tr>
                                    </thead>
                                    <tbody>
                                        <c:forEach items="${listaReqsPTaxEsp}" var="reqs">
                                            <tr>
                                                <td>${reqs.pnome_clt} ${reqs.snome_clt}</td>
                                                <td>${reqs.tel_clt}</td>
                                                <td>${reqs.sexo_clt}</td>
                                                <td>${reqs.dtNasc_clt}</td>
                                                <td>( ${reqs.lat_req} , ${reqs.lng_req} )</td>
                                                <td>
                                                    <a href="${pageContext.servletContext.contextPath}/usuario/escolheuCorrida?id=${reqs.id_req}&esc=true">Aceitar</a>
                                                </td>
                                                <td>
                                                    <a href="${pageContext.servletContext.contextPath}/usuario/escolheuCorrida?id=${reqs.id_req}&esc=false">Recusar</a>
                                                </td>
                                            </tr>
                                        </c:forEach>
                                    </tbody>
                                </table>
                            </div>
                        </div>
                    </c:if>
                    <br>
                    
                    <br>
                    <form method="post" action="${pageContext.servletContext.contextPath}/corrida/atualizar" >
                        
                        <input type="submit" value="Atualizar" class="btn btn-primary" />
                        
                    </form>
                    
                    <div id="map">

                    </div>
                </div>
            </div>
            <%@include file="/view/includes/rodape.jsp" %>
        </div>
        <%@include file="/view/includes/scripts.jsp" %>
        <c:choose>
        <c:when test="${isTaxista}">
            
        </c:when>
        <c:otherwise>
            <script type="text/javascript" src="${pageContext.servletContext.contextPath}/assets/js/mapDesloc.js"></script>
            <script src="https://maps.googleapis.com/maps/api/js?key=AIzaSyB-BQI2fSVPRZ_vKa9DAUDv9VATDQfpd8k&callback=initMap" async defer></script>
        </c:otherwise>
        </c:choose>
    </body>
</html>
