<%@page contentType="text/html" pageEncoding="UTF-8"%>

<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>

<c:if test="${empty sessionScope.usuario}">
    <c:redirect context="${pageContext.servletContext.contextPath}" url="/" ></c:redirect>
</c:if>

<!DOCTYPE html>
<html>
    <head>
        <%@include file="/view/includes/head.jsp" %>
    </head>
    <body>
        <div class="container">

            <div class="row">
                <div class="col-md-10 col-xs-10">
                    <%@include file="/view/includes/menu.jsp" %>
                </div>
            </div>

            <div class="row">
                <div class="col-md-10">
                    
                    <c:choose>
                    <c:when test="${isTaxista}">
                        
                        <form method="post" action="${pageContext.servletContext.contextPath}/corrida/notaTaxista" >

                            <div class="form-group">
                                <label> Atribua uma nota em relação ao usuário: </label>
                                <input type="text" name="notaSatsTax" class="form-control" />
                            </div>

                            <input type="submit" value="Atribuir" class="btn btn-default" />
                        </form>
                        
                    </c:when>
                    <c:otherwise>
                        
                        <p> Ola ${usuario.pnome} ${usuario.snome} </p>
                        <br>
                        <p> Suas informacoes: </p>

                        <li><p>TELEFONE: ${usuario.tel}</p></li>
                        <li><p>SEXO: ${usuario.sexo}</p></li>
                        <li><p>CPF: ${usuario.cpf}</p></li>
                        <li><p>LOGIN: ${usuario.login}</p></li>
                        <li><p>DATA DE NASCIMENTO: ${usuario.dtNasc}</p></li>
                        <br>
                        <p> Informações do taxista: </p>
                        <br>
                        <li><p>NOME: ${taxista.pnome} ${taxista.snome}</p></li>
                        <li><p>TELEFONE: ${taxista.tel}</p></li>
                        <li><p>SEXO: ${taxista.sexo}</p></li>
                        <li><p>CPF: ${taxista.cpf}</p></li>
                        <li><p>LOGIN: ${taxista.login}</p></li>
                        <li><p>DATA DE NASCIMENTO: ${taxista.dtNasc}</p></li>
                        <li><p>VALOR COBRADO: ${taxista.valor_cpkr}</p></li>
                        <br>
                        <p> Informações da corrida: </p>
                        <br>
                        <li><p>DISTANCIA PERCORRIDA: ${corridaAt.distancia} Km </p></li>
                        <li><p>VALOR TOTAL A PAGAR: R$ ${corridaAt.valor}</p></li>
                        <br>
                        
                        <form method="post" action="${pageContext.servletContext.contextPath}/corrida/notaCliente" >

                            <div class="form-group">
                                <label> Dê uma nota de 1 a 5 de sua experiencia: </label>
                                <input type="text" name="notaSatsCl" class="form-control" />
                            </div>

                            <input type="submit" value="Confirmar pagamento e atribuir nota" class="btn btn-default" />
                        </form>
                        
                    </c:otherwise>
                    </c:choose>
                    
                </div>
            </div>
            <%@include file="/view/includes/rodape.jsp" %>
            
        </div>
        <%@include file="/view/includes/scripts.jsp" %>
    </body>
</html>
