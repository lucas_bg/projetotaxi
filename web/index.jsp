<%@page contentType="text/html" pageEncoding="UTF-8"%>

<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>

<c:if test="${not empty sessionScope.usuario}">
    <c:redirect context="${pageContext.servletContext.contextPath}" url="/usuario" />
</c:if>

<!DOCTYPE html>
<html>
    <head>
        <title>Index</title>
        <%@include file="/view/includes/head.jsp" %>
    </head>
    <body>
        <div class="container">
            <div class="row">
                <div class="col-md-10 col-xs-10">
                    <%@include file="/view/includes/menu.jsp"%>
                </div>
            </div>
                
            <img src="http://www.folhavitoria.com.br/geral/blogs/dia-da-mulher/wp-content/uploads/2015/03/Mapa-Easy-Taxi.png"
                 width="700" height="400">
            
            <%@include file="/view/includes/rodape.jsp" %>
        </div>
        <%@include file="/view/includes/scripts.jsp" %>
    </body>
</html>
