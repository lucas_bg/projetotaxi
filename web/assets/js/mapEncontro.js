/* global google */

var map2;

function initMap() {
    map2 = new google.maps.Map(document.getElementById('map'), {
        center: {lat: 39.82533, lng: 116.71878},
        zoom: 11
    });
}

function createMarker(pos, t, pt, it) {
    var cor = '';
    if(it===0){
        cor = 'http://maps.google.com/mapfiles/ms/icons/green-dot.png';
    }else {
        cor = 'http://maps.google.com/mapfiles/ms/icons/purple-dot.png';
    }
    
    var marker = new google.maps.Marker({
        position: pos,
        map: map2,
        title: t,
        icon: cor
    });
    
    var contentString = '<div id="content">'+
    '<div id="bodyContent">'+
    '<a href="SistemaTaxi/corrida/escolheuDestino?id=' + pt.id + '">Escolher</a>' +
    '</div>'+
    '</div>';

    var infowindow = new google.maps.InfoWindow({
        content: contentString
    });
    
    google.maps.event.addListener(marker, 'click', function() {
        infowindow.open(map2, marker);
    });
    
    return marker;
}

$(document).ready(function() {
    var myMarkers = [];
    
    /*
    $.get("SistemaTaxi/corrida/encontroJS", function (trajPt2) {
        for (var it = 0; it < trajPt2.length; it++) {
            var myLatLng = {lat: trajPt2[it].lat, lng: trajPt2[it].lng};
            var m = createMarker(myLatLng, it.toString(), trajPt2[it], it);
            console.log(myLatLng);
            myMarkers.push(myLatLng);
        }
    });
    */
    $.ajax({ url: 'SistemaTaxi/corrida/encontroJS', 
         async: false,
         dataType: 'json',
         success: function (trajPt2) {
                for (var it = 0; it < trajPt2.length; it++) {
                    var myLatLng = {lat: trajPt2[it].lat, lng: trajPt2[it].lng};
                    var m = createMarker(myLatLng, it.toString(), trajPt2[it], it);
                    console.log(myLatLng);
                    myMarkers.push(myLatLng);
                }
            }
        });
    console.log(myMarkers);
    var tPath = new google.maps.Polyline({
        path: myMarkers,
        geodesic: true,
        strokeColor: '#FF0000',
        strokeOpacity: 1.0,
        strokeWeight: 2
    });
    tPath.setMap(map2);
});
