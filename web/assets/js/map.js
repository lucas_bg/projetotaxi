/* global google */

var map;

function initMap() {
    var directionsDisplay, directionsService;
    directionsService = new google.maps.DirectionsService();
    directionsDisplay = new google.maps.DirectionsRenderer();
    
    map = new google.maps.Map(document.getElementById('map'), {
        center: {lat: 39.82533, lng: 116.71878},
        zoom: 8
    });
    
    directionsDisplay.setMap(map);
    
    map.addListener('click', function(e) {
        var latT = e.latLng.lat();
        var lngT = e.latLng.lng();
        document.getElementById("latEscCl2").value = latT.toString();
        document.getElementById("lngEscCl2").value = lngT.toString();
    });
}

//icon: 'http://maps.google.com/mapfiles/ms/icons/blue-dot.png'

function createMarker(pos, t) {
    var marker = new google.maps.Marker({
        position: pos,
        map: map,
        title: t
    });
    
    google.maps.event.addListener(marker, 'click', function() {
        $.get("SistemaTaxi/usuario/escolheuPonto?id=" + marker.getTitle(), function() {
             //document.getElementById("waitRespId").style.visibility = "visible";
             location.reload(true);
        });
    });
    
    return marker;
}

$(document).ready(function() {
    //document.getElementById("waitRespId").style.visibility = "hidden";
    document.getElementById("soPEsconder").style.visibility = "hidden";
    document.getElementById("latEscCl").style.visibility = "hidden";
    document.getElementById("lngEscCl").style.visibility = "hidden";
    //document.getElementById("latEscCl").style.visibility = "visible";
    //document.getElementById("lngEscCl").style.visibility = "visible";
    
    $("#buttonTest").click(function(){
        //var latEsc = document.getElementById("latEscCl").value;
        var latEsc2 = document.getElementById("latEscCl2").value;
        //var lngEsc = document.getElementById("lngEscCl").value;
        var lngEsc2 = document.getElementById("lngEscCl2").value;

        $.get("SistemaTaxi/usuario/reqCorr?lat=" + latEsc2 + "&lng=" + lngEsc2, function (positions) {
            for (var it = 0; it < positions.length; it++) {
                var myLatLng = {lat: positions[it].head.lat, lng: positions[it].head.lng};
                var m = createMarker(myLatLng, it.toString());
            }
        });
    });
});
