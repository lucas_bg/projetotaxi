/* global google */

var map2;

function initMap() {
    map2 = new google.maps.Map(document.getElementById('map'), {
        center: {lat: 39.82533, lng: 116.71878},
        zoom: 11
    });
}

function createMarker(pos, t) {
    var marker = new google.maps.Marker({
        position: pos,
        map: map2,
        title: t,
        icon: 'http://maps.google.com/mapfiles/ms/icons/green-dot.png'
    });
    
    return marker;
}

$(document).ready(function() {
    
    $.get("SistemaTaxi/corrida/atualizaDesloc", function (pontoAtualTaxi) {
        var myLatLng = {lat: pontoAtualTaxi.lat, lng: pontoAtualTaxi.lng};
        var m = createMarker(myLatLng, 1);
    });
    /*
    $.ajax({ url: 'SistemaTaxi/corrida/encontroJS', 
         async: false,
         dataType: 'json',
         success: function (trajPt2) {
                for (var it = 0; it < trajPt2.length; it++) {
                    var myLatLng = {lat: trajPt2[it].lat, lng: trajPt2[it].lng};
                    var m = createMarker(myLatLng, it.toString(), trajPt2[it], it);
                    console.log(myLatLng);
                    myMarkers.push(myLatLng);
                }
            }
        });
        */
});
