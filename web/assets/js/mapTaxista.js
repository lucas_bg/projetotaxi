/* global google */

var map;

function initMap() {
    //var directionsDisplay, directionsService;
    //directionsService = new google.maps.DirectionsService();
    //directionsDisplay = new google.maps.DirectionsRenderer();
    
    map = new google.maps.Map(document.getElementById('map'), {
        center: {lat: 39.82533, lng: 116.71878},
        zoom: 8
    });
    
    //directionsDisplay.setMap(map);
    
    map.addListener('click', function(e) {
        var latT = e.latLng.lat();
        var lngT = e.latLng.lng();
        document.getElementById("latEscCl").value = latT.toString();
        document.getElementById("lngEscCl").value = lngT.toString();
    });
}

//icon: 'http://maps.google.com/mapfiles/ms/icons/blue-dot.png'

function createMarker(pos, t, req) {
    var marker = new google.maps.Marker({
        position: pos,
        map: map,
        title: t,
        icon: 'http://maps.google.com/mapfiles/ms/icons/blue-dot.png'
    });
    
    var contentString = '<div id="content">'+
    '<div id="bodyContent">'+
    '<p> Cliente: ' + req.pnome_clt + ' ' + req.snome_clt + '</p>' +
    '<p> Telefone: ' + req.tel_clt + '</p>' +
    '<p> Sexo: ' + req.sexo_clt + '</p>' +
    '<p> Data de Nascimento: ' + req.dtNasc_clt + '</p>' +
    '<a href="SistemaTaxi/usuario/escolheuCorrida?id='+ req.id_req +'&esc=true">Aceitar</a>' +
    '<br>' +
    '<a href="SistemaTaxi/usuario/escolheuCorrida?id='+ req.id_req +'&esc=false">Recusar</a>' +
    '</div>'+
    '</div>';

    var infowindow = new google.maps.InfoWindow({
        content: contentString
    });
    
    google.maps.event.addListener(marker, 'click', function() {
        infowindow.open(map, marker);
    });
    
    return marker;
}

function createMarker2(pos, t, req) {
    var marker2 = new google.maps.Marker({
        position: pos,
        map: map,
        title: t,
        icon: 'http://maps.google.com/mapfiles/ms/icons/yellow-dot.png'
    });
    
    var contentString = "";
    
    console.log("caso 2");
    contentString = '<div id="content">'+
    '<div id="bodyContent">'+
    '<p> Cliente: ' + req.pnome_clt + ' ' + req.snome_clt + '</p>' +
    '<p> Telefone: ' + req.tel_clt + '</p>' +
    '<p> Sexo: ' + req.sexo_clt + '</p>' +
    '<p> Data de Nascimento: ' + req.dtNasc_clt + '</p>' +
    '<a href="SistemaTaxi/corrida/comecouCorridapt2?id=' + req.id_corr +'">Começar</a>' +
    '</div>'+
    '</div>'; 
    
    var infowindow = new google.maps.InfoWindow({
        content: contentString
    });
    
    google.maps.event.addListener(marker2, 'click', function() {
        infowindow.open(map, marker2);
    });
    
    return marker2;
}

$(document).ready(function() {
    //document.getElementById("waitRespId").style.visibility = "hidden";
    document.getElementById("soPEsconder").style.visibility = "hidden";
    document.getElementById("latEscCl").style.visibility = "hidden";
    document.getElementById("lngEscCl").style.visibility = "hidden";
    //document.getElementById("latEscCl").style.visibility = "visible";
    //document.getElementById("lngEscCl").style.visibility = "visible";
    
    $.get("SistemaTaxi/usuario/taxVerifPorReqs", function (listaReqsPTaxEsp) {
        for (var it = 0; it < listaReqsPTaxEsp.length; it++) {
            var myLatLng = {lat: listaReqsPTaxEsp[it].lat_req, lng: listaReqsPTaxEsp[it].lng_req};
            var m = createMarker(myLatLng, it.toString(), listaReqsPTaxEsp[it]);
        }
    });
    /*
    $.ajax({ url: 'SistemaTaxi/usuario/taxVerifPorReqs', 
        async: false,
        dataType: 'json',
        success: function (listaReqsPTaxEsp) {
            for (var it = 0; it < listaReqsPTaxEsp.length; it++) {
                var myLatLng = {lat: listaReqsPTaxEsp[it].lat_req, lng: listaReqsPTaxEsp[it].lng_req};
                var m = createMarker(myLatLng, it.toString(), listaReqsPTaxEsp[it]);
            }
        }
    });
    */
    $.get("SistemaTaxi/usuario/taxVerifCorrAComecar", function (listaCorr) {
        for (var it = 0; it < listaCorr.length; it++) {
            var myLatLng = {lat: listaCorr[it].lat_dest, lng: listaCorr[it].lng_dest};
            var m = createMarker2(myLatLng, it.toString(), listaCorr[it]);
        }
    });
    
    
    $("#buttonTest").click(function(){
        //var latEsc = document.getElementById("latEscCl").value;
        var latEsc2 = document.getElementById("latEscCl2").value;
        //var lngEsc = document.getElementById("lngEscCl").value;
        var lngEsc2 = document.getElementById("lngEscCl2").value;

        $.get("SistemaTaxi/usuario/reqCorr?lat=" + latEsc2 + "&lng=" + lngEsc2, function (positions) {
            for (var it = 0; it < positions.length; it++) {
                var myLatLng = {lat: positions[it].head.lat, lng: positions[it].head.lng};
                var m = createMarker(myLatLng, it.toString());
            }
        });
    });
    
});
